<div>
<h4 align="center">This project has transfered to GitHub! Get the up-to-date version here at <a href="https://github.com/dreamilization/Acadia">GitHub</a>!
<h4 align="center">This project is planned to be deprecated on Oct. 1st 2019.

Project Acadia
-----------------
[![Pipeline Status](https://gitlab.com/chengfeiwu/Acadia/badges/master/pipeline.svg)](https://gitlab.com/chengfeiwu/Acadia/commits/master)
[![GitHub](https://img.shields.io/github/license/dreamilization/GraduateCountdown.svg?label=License)](https://gitlab.com/chengfeiwu/Acadia/blob/master/LICENSE)
==================
A JavaFX based game.

**Notes and Known Issues**
- Cross Platform:
    - All platforms have been supported.
- Game Save & Load:
    - Load and save game is now supported.
- New Game Setup:
    - User will be able to set their own nickname.
- Core Render Engine:
    - Map can be displayed by using a graphic meta data.
    <br>**Known Issues:**
        - Limited amount of objects can be displayed. 
- JDK 11:
    - This project supports JDK 11 and below.
- Multi-Language:
    <br>**Known Issues:**
    - Implementation pending. 

**Contributors**
- Scott H. - [ScottHamper](https://github.com/ScottHamper)

**Versioning**

We use [Git](https://git-scm.com/) for versioning.

**Authors**

* **William W.** - *Initial work* - [chengfeiwu](https://gitlab.com/chengfeiwu)

**Copyright**

Copyright &copy; 2017-2019, William Wu. Released under the [GNU GPLv3 License](LICENSE).

