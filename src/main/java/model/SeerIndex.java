package model;

public enum SeerIndex
{
    BobSeed(20, 1, "BobSeed"),
    BobGrass(40, 2, "BobGrass"),
    BobFlower(60, 3, "BobFlower");

    private int upgradeLevel;
    private int indexID;
    private String commonName;

    private SeerIndex(int upgradeLevel, int indexID, String commonName)
    {
        this.upgradeLevel = upgradeLevel;
        this.indexID = indexID;
        this.commonName = commonName;
    }

    public int getUpgradeLevel()
    {
        return upgradeLevel;
    }

    public int getIndexID()
    {
        return indexID;
    }

    public String getCommonName()
    {
        return commonName;
    }
}
