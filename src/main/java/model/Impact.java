package model;

import java.util.Random;

public enum Impact
{
    Freeze(0.08, 4),
    Burn(0.08, 4, Freeze),
    Hunger(0.7, 4),
    Shock(0.08, 4),
    Dizzy(0.0, 4),
    Fear(0.5, 4);

    private final SpecialImpact specialImpact;
    private Impact against;

    Impact(double percent, int round)
    {
        specialImpact = new SpecialImpact(percent, round);
    }

    Impact(double percent, int round, Impact impact)
    {
        specialImpact = new SpecialImpact(percent, round);
        against = impact;
    }

    public boolean isSkipRound()
    {
        if (this.equals(Shock) || this.equals(Dizzy))
        {
            return true;
        }
        else if (this.equals(Fear))
        {
            int tmp = new Random().nextInt(100);
            return tmp <= 45;
        }
        return false;
    }

    public int getAttackPointImpact(int originalAttackValue)
    {
        if (this.equals(Fear) || this.equals(Hunger))
        {
            return specialImpact.getAmount(originalAttackValue);
        }
        return 0;
    }

    public int getHealthPointImpact(int originalHealthPoint)
    {
        if (this.equals(Shock) || this.equals(Burn) || this.equals(Freeze))
        {
            return specialImpact.getAmount(originalHealthPoint);
        }
        return 0;
    }

    public boolean isAgainst(Impact impact)
    {
        return impact == against;
    }

    public void decreaseRound()
    {
        specialImpact.decreaseRound();
    }

    public int getRoundLeft()
    {
        return specialImpact.getRoundLeft();
    }
}
