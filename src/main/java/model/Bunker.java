package model;

public class Bunker extends Building
{
    public Bunker()
    {
        super();
    }

    public Bunker(final boolean isWallPiece)
    {
        super(isWallPiece);
    }

    public void interact(final User user)
    {
        if (!isInteractive())
        {
            throw new UnsupportedOperationException();
        }

        // FIXME
        System.out.println("Ready to enter bunker. (New Screen required)");
    }
}
