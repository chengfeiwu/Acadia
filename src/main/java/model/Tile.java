package model;

public class Tile
{
    private final Terrain terrain;
    private final TileObject tileObject;

    public Tile(final Terrain terrain, final TileObject tileObject)
    {
        this.terrain = terrain;
        this.tileObject = tileObject;
    }

    public Terrain terrain()
    {
        return terrain;
    }

    public TileObject tileObject()
    {
        return tileObject;
    }

    public boolean hasTileObject()
    {
        return tileObject != null;
    }
}