package model;

public enum SkillIndex
{
    Jump(new Skill(
        "Jump",
        5,
        20,
        null,
        0.0,
        0.4,
        10,
        "5 damage to the opponent")),

    DieForMe(new Skill(
        "Die For Me!",
        0,
        5,
        null,
        0.0,
        0.2,
        9999,
        "20% chance of instance kill")),

    LastSurprise(new Skill(
        "The Last Surprise",
        240,
        5,
        Impact.Fear,
        0.2,
        0.1,
        400,
        "240 damage and 10% chance to fear the opponent"));

    private Skill skill;

    SkillIndex(Skill skill)
    {
        this.skill = skill;
    }

    public Skill getSkill()
    {
        return skill;
    }
}
