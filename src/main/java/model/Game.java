package model;

import java.util.HashMap;

public class Game
{
    private User user;
    private Map map;
    private Vector2D playerPosition;
    private java.util.Map<String, Boolean> conversationRecord = new HashMap<>();

    public Game()
    {
        this(new User(), null);
    }

    public Game(final User user)
    {
        this(user, null);
    }

    public Game(final User user, final Map map)
    {
        this.user = user;
        this.map = map;
        playerPosition = Vector2D.ZERO;
        conversationRecord.put("introConversation", false);
    }

    public User user()
    {
        return user;
    }

    public Map map()
    {
        return map;
    }

    public void triggerConversation(String record)
    {
        if (conversationRecord.containsKey(record))
        {
            conversationRecord.put(record, true);
        }
        else
        {
            throw new IllegalStateException(
                "Current Key: " + record + " doesn't have a match up in the HashMap.");
        }
    }

    public boolean canTriggerConversation(String record)
    {
        if (conversationRecord.containsKey(record))
        {
            return conversationRecord.get(record);
        }

        throw new IllegalStateException(
            "Current Key: " + record + " doesn't have a match up in the HashMap.");
    }

    public void setMap(Map map)
    {
        if (this.map == null)
        {
            this.map = map;
        }
    }

    public boolean isValid()
    {
        return user != null && map != null;
    }

    public Vector2D playerPosition()
    {
        return playerPosition;
    }

    public boolean canPlayerMove(final Direction direction)
    {
        final var position = playerPosition.add(direction.toVector2D());

        return position.x() >= 0
            && position.x() < map.width()
            && position.y() >= 0
            && position.y() < map.height()
            && (map.tileAt(position).tileObject() == null
                || !map.tileAt(position).tileObject().isObstacle(direction));
    }

    public void movePlayer(final Vector2D position)
    {
        playerPosition = playerPosition.add(position);
    }
}