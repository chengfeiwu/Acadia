package model;

public abstract class Building implements TileObject
{
    private boolean isWallPiece;

    public Building()
    {
        this(true);
    }

    public Building(final boolean isWallPiece)
    {
        this.isWallPiece = isWallPiece;
    }

    public void setIsWallPiece(final boolean isWallPiece)
    {
        this.isWallPiece = isWallPiece;
    }

    public boolean isInteractive()
    {
        return !isWallPiece;
    }

    public boolean isObstacle(final Direction direction)
    {
        // If the building object is not a wall piece, then
        // it's a door and we only want to allow the player
        // to move over it when they're moving upward.
        return isWallPiece
            || direction != Direction.Up;
    }
}
