package model;

public enum Terrain
{
    Grass,
    SandRoad,
    Debug,
    Water,
    RegularRoad,
    BunkerRoad,
    Plain;
}
