package model;

import java.util.Objects;

public class Vector2D
{
    public static final Vector2D ZERO = new Vector2D(0, 0);

    private final int x;
    private final int y;

    public Vector2D(final int x, final int y)
    {
        this.x = x;
        this.y = y;
    }

    public int x()
    {
        return x;
    }

    public int y()
    {
        return y;
    }

    public Vector2D add(final Vector2D other)
    {
        return new Vector2D(x + other.x, y + other.y);
    }

    @Override
    public String toString()
    {
        return String.format("(%d, %d)", x, y);
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Vector2D other = (Vector2D)o;

        return x == other.x
            && y == other.y;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(x, y);
    }
}