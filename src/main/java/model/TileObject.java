package model;

public interface TileObject
{
    boolean isInteractive();

    boolean isObstacle(Direction direction);

    void interact(User user);
}
