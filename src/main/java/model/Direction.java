package model;

public enum Direction
{
    Left,
    Right,
    Up,
    Down;

    public Vector2D toVector2D()
    {
        switch (this)
        {
            case Left: return new Vector2D(-1, 0);
            case Right: return new Vector2D(1, 0);
            case Up: return new Vector2D(0, -1);
            case Down: return new Vector2D(0, 1);
            default:
                throw new UnsupportedOperationException();
        }
    }
}
