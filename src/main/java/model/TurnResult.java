package model;

public class TurnResult
{
    private final CommandResult firstResult;
    private final CommandResult secondResult;

    public TurnResult(final CommandResult firstResult, final CommandResult secondResult)
    {
        this.firstResult = firstResult;
        this.secondResult = secondResult;
    }

    public CommandResult getFirstResult()
    {
        return firstResult;
    }

    public CommandResult getSecondResult()
    {
        return secondResult;
    }
}
