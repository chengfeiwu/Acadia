package model;

import seer.Seer;

public class CommandResult
{
    public static CommandResult failure(BattleOperation operation, final Seer performSeer)
    {
        final CommandResult result = new CommandResult(operation, performSeer);
        result.wasExecuted = false;

        return result;
    }

    private BattleOperation operation;
    private boolean isCriticalHit;
    private boolean isSelfHeal;
    private boolean isSelfImpacted;
    private boolean isImpacted;
    private boolean wasExecuted;
    private Seer performOrUseOnSeer;
    private Seer defendSeer;
    private User player;
    private Object move;

    public CommandResult(BattleOperation operation, final Seer performOrUseOnSeer)
    {
        this.operation = operation;
        this.performOrUseOnSeer = performOrUseOnSeer;
        wasExecuted = true;
    }

    public BattleOperation getOperation()
    {
        return operation;
    }

    public boolean isCriticalHit()
    {
        return isCriticalHit;
    }

    public Object getMove()
    {
        return move;
    }

    public void setMove(Object move)
    {
        this.move = move;
    }

    public void setCriticalHit(boolean criticalHit)
    {
        isCriticalHit = criticalHit;
    }

    public boolean isSelfImpacted()
    {
        return isSelfImpacted;
    }

    public Seer getPerformOrUseOnSeer()
    {
        return performOrUseOnSeer;
    }

    public void setPerformOrUseOnSeer(Seer performOrUseOnSeer)
    {
        this.performOrUseOnSeer = performOrUseOnSeer;
    }

    public Seer getDefendSeer()
    {
        return defendSeer;
    }

    public void setDefendSeer(Seer defendSeer)
    {
        this.defendSeer = defendSeer;
    }

    public void setSelfImpacted(boolean selfImpacted)
    {
        isSelfImpacted = selfImpacted;
    }

    public boolean isSelfHeal()
    {
        return isSelfHeal;
    }

    public void setSelfHeal(boolean selfHeal)
    {
        isSelfHeal = selfHeal;
    }

    public boolean isImpacted()
    {
        return isImpacted;
    }

    public void setImpacted(boolean impacted)
    {
        isImpacted = impacted;
    }

    public boolean wasExecuted()
    {
        return wasExecuted;
    }

    public CommandResult setPlayer(final User player)
    {
        this.player = player;
        return this;
    }
}
