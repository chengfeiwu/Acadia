package model;

import java.util.Random;

public class Bush implements TileObject
{
    private final Random random;

    public Bush()
    {
        random = new Random();
    }

    public boolean isInteractive()
    {
        return true;
    }

    public boolean isObstacle(final Direction direction)
    {
        return false;
    }

    public void interact(final User user)
    {
        if (random.nextInt(9) < 3)
        {
            // FIXME
            System.out.println("In");
        }
    }
}
