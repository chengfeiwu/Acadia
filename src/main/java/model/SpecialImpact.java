package model;

public class SpecialImpact
{
    public static final int INFINITE = 99999;

    private int amount;
    private double amountPercent;
    private int roundLeft;

    public SpecialImpact(int amount, int roundLeft)
    {
        this.amount = amount;
        this.roundLeft = roundLeft;
    }

    public SpecialImpact(double amountPercent, int roundLeft)
    {
        this.amountPercent = amountPercent;
        this.roundLeft = roundLeft;
    }

    public int getAmount(int originalValue)
    {
        return (int) (amountPercent * originalValue);
    }

    public int getRoundLeft()
    {
        return roundLeft;
    }

    public void decreaseRound()
    {
        roundLeft--;
    }
}
