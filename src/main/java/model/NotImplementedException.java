package model;

public final class NotImplementedException extends RuntimeException
{
    public NotImplementedException(String errorMessage)
    {
        super(errorMessage);
    }

    public NotImplementedException()
    {
        super();
    }
}
