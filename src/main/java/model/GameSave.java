package model;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public class GameSave
{
    private final Settings settings;
    private Game game;
    private Instant timestamp;

    public GameSave()
    {
        settings = new Settings();
        game = new Game();

        // FIXME: Get timestamp from the last modified date in the OS filesystem
        timestamp = Instant.now();
    }

    public Settings getSettings()
    {
        return settings;
    }

    public Game game()
    {
        return game;
    }

    public void setGame(Game game)
    {
        this.game = game;
    }

    public void setTime()
    {
        timestamp = Instant.now();
    }

    public LocalDate date()
    {
        return timestamp.atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
