package model;

import item.Item;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import seer.Seer;

public class User
{
    private static final int MAX_NICKNAME_LENGTH = 15;

    private final int unitNumber;
    private String nickname;
    private int gold;
    private Seer[] carriedSeers = new Seer[6];
    private final Map<Item, Integer> items; //Key represent item ID, Value represent amount.
    private final List<Seer> benchedSeers;

    public User()
    {
        this(new Random().nextInt(10000));
    }

    public User(final int unitNumber)
    {
        this("9S_" + unitNumber, unitNumber);
    }

    public User(final String nickname, final int unitNumber)
    {
        this.unitNumber = unitNumber;
        setNickname(nickname);
        gold = 1000;
        items = new HashMap<>();
        benchedSeers = new ArrayList<>();
    }

    public void upgradeSeer(final Seer seer)
    {

    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(final String nickname)
    {
        this.nickname = nickname.length() < MAX_NICKNAME_LENGTH
            ? nickname
            : nickname.substring(0, MAX_NICKNAME_LENGTH);
    }

    public int getUnitNumber()
    {
        return unitNumber;
    }

    public void addItems(Item... item)
    {
        for (Item i : item)
        {
            if (hasItem(i))
            {
                int newAmount = amountLeft(i) + 1;
                items.put(i, newAmount);
            }
            else
            {
                items.put(i, 1);
            }
        }
    }

    public int getGold()
    {
        return gold;
    }

    public boolean hasItem(Item item)
    {
        return items.containsKey(item);
    }

    public int amountLeft(Item item)
    {
        return items.get(item);
    }

    public void itemUsed(Item item)
    {
        if (!hasItem(item))
        {
            throw new UnsupportedOperationException("Item doesn't exist in the user's list.");
        }

        int neew = amountLeft(item) - 1;
        if (neew == 0)
        {
            items.remove(item);
        }
        else
        {
            items.put(item, neew);
        }
    }

    public Map<Item, Integer> getItems()
    {
        return Collections.unmodifiableMap(items);
    }

    public List<Seer> getBenchedSeers()
    {
        return Collections.unmodifiableList(benchedSeers);
    }
}
