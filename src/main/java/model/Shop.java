package model;

public class Shop extends Building
{
    public Shop()
    {
        super();
    }

    public Shop(final boolean isWallPiece)
    {
        super(isWallPiece);
    }

    public void interact(final User user)
    {
        if (!isInteractive())
        {
            throw new UnsupportedOperationException();
        }

        // FIXME
        System.out.println("Ready to enter shop. (New Screen required)");
    }
}
