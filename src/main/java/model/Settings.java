package model;

import javafx.stage.StageStyle;

public class Settings
{
    private double masterVolume = 1.0;
    private StageStyle stageStyle = StageStyle.DECORATED;

    private String region =
        System.getProperty("user.language") + "_" + System.getProperty("user.country");

    public void volumeDown()
    {
        masterVolume = masterVolume > 0 ? masterVolume - 0.1 : masterVolume;
    }

    public void volumeUp()
    {
        masterVolume = masterVolume < 1.0 ? masterVolume + 0.1 : masterVolume;
    }

    public double getVolume()
    {
        return masterVolume;
    }

    public void setStageStyle(final StageStyle stageStyle)
    {
        this.stageStyle = stageStyle;
    }

    public String getLanguageAndRegion()
    {
        return region;
    }

    public StageStyle getStageStyle()
    {
        return stageStyle;
    }
}
