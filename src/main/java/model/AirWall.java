package model;

public class AirWall implements TileObject
{
    private boolean isObstacle;

    public AirWall()
    {
        this(true);
    }

    public AirWall(final boolean isObstacle)
    {
        this.isObstacle = isObstacle;
    }

    public boolean isInteractive()
    {
        return true;
    }

    public boolean isObstacle(final Direction direction)
    {
        return isObstacle;
    }

    public void interact(final User user)
    {
        isObstacle = true;
    }
}
