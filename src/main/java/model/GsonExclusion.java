package model;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class GsonExclusion implements ExclusionStrategy
{
    private final Class<?> typeToSkip;

    public GsonExclusion(Class<?> typeToSkip)
    {
        this.typeToSkip = typeToSkip;
    }

    public boolean shouldSkipClass(Class<?> clazz)
    {
        return clazz.equals(typeToSkip);
    }

    public boolean shouldSkipField(FieldAttributes f)
    {
        return f.getAnnotation(Exclude.class) != null;
    }
}
