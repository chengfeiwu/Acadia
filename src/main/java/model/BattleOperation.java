package model;

public enum BattleOperation
{
    Fight,
    Seer,
    Bag,
    Run,
}
