package model;

public class StatModifier
{
    private int attackChangePercent;
    private int defendsChangePercent;
    private int speedChangePercent;

    public StatModifier()
    {

    }

    public StatModifier(int attackChangePercent, int defendsChangePercent, int speedChangePercent)
    {
        this.attackChangePercent = attackChangePercent;
        this.defendsChangePercent = defendsChangePercent;
        this.speedChangePercent = speedChangePercent;
    }

    public int getAttackChangePercent()
    {
        return attackChangePercent;
    }

    public int getDefendsChangePercent()
    {
        return defendsChangePercent;
    }

    public int getSpeedChangePercent()
    {
        return speedChangePercent;
    }

    public void applyTo(StatModifier destinationStatModifier)
    {
        destinationStatModifier.modify(
            attackChangePercent,
            defendsChangePercent,
            speedChangePercent);
    }

    public void modify(int attackChangePercent, int defendsChangePercent, int speedChangePercent)
    {
        this.attackChangePercent += attackChangePercent;
        this.defendsChangePercent += defendsChangePercent;
        this.speedChangePercent += speedChangePercent;
    }
}
