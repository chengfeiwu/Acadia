package model;

public class Map
{
    // Outer array is columns
    // Inner arrays are rows
    private final Tile[][] map;

    public Map(final Tile[][] map)
    {
        this.map = map;
    }

    public Tile tileAt(final Vector2D position)
    {
        return tileAt(position.x(), position.y());
    }

    public Tile tileAt(final int x, final int y)
    {
        return map[x][y];
    }

    public int width()
    {
        return map.length;
    }

    public int height()
    {
        return map.length == 0 ? 0 : map[0].length;
    }
}
