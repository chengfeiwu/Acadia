package model;

import java.util.Random;
import seer.Seer;

public class Skill
{
    private String name;
    private int damage;
    private int maxPP;
    private Impact specialImpact;
    private double chanceOfSpecialImpact;
    private double chanceOfCritical;
    private int criticalDamage;
    private String description;

    public Skill(
        String name,
        int damage,
        int maxPP,
        Impact specialImpact,
        double chanceOfSpecialImpact,
        double chanceOfCritical,
        int criticalDamage)
    {
        this.name = name;
        this.damage = damage;
        this.maxPP = maxPP;
        this.specialImpact = specialImpact;
        this.chanceOfSpecialImpact = chanceOfSpecialImpact;
        this.chanceOfCritical = chanceOfCritical;
        this.criticalDamage = criticalDamage;
        description = "No description for this Skill";
    }

    public Skill(
        String name,
        int damage,
        int maxPP,
        Impact specialImpact,
        double chanceOfSpecialImpact,
        double chanceOfCritical,
        int criticalDamage,
        String description)
    {
        this.name = name;
        this.damage = damage;
        this.maxPP = maxPP;
        this.specialImpact = specialImpact;
        this.chanceOfSpecialImpact = chanceOfSpecialImpact;
        this.chanceOfCritical = chanceOfCritical;
        this.criticalDamage = criticalDamage;
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public int getChanceOfCritical()
    {
        return (int) (chanceOfCritical * 100);
    }

    public int getCriticalDamage()
    {
        return criticalDamage;
    }

    public int getDamage()
    {
        return damage;
    }

    public String getName()
    {
        return name;
    }

    public int getMaxPP()
    {
        return maxPP;
    }

    public CommandResult performAttack(Seer offend, Seer defend)
    {
        CommandResult records = new CommandResult(BattleOperation.Fight, offend);
        records.setMove(this);

        if (damage < 0)
        {
            offend.receiveAttack(damage, null);
            records.setSelfHeal(true);
            records.setDefendSeer(offend);
            return records;
        }

        int finalDamage = 0;
        Impact statusChange = null;

        records.setDefendSeer(defend);

        int random = new Random().nextInt(100);
        if (random < getChanceOfCritical())
        {
            finalDamage += getCriticalDamage();
            records.setCriticalHit(true);
        }
        else
        {
            finalDamage += getDamage();
        }

        if (getSpecialImpact() != null)
        {
            random = new Random().nextInt(100);
            if (random < getChanceOfSpecialImpactPercentage())
            {
                statusChange = getSpecialImpact();
                records.setImpacted(true);
            }
        }

        Impact offendStatus = offend.getStatus();
        if (offendStatus != null)
        {
            finalDamage -= offendStatus.getAttackPointImpact(finalDamage);
        }

        defend.receiveAttack(finalDamage, statusChange);
        return records;
    }

    public Impact getSpecialImpact()
    {
        return specialImpact;
    }

    public int getChanceOfSpecialImpactPercentage()
    {
        return (int) (chanceOfSpecialImpact * 100);
    }
}
