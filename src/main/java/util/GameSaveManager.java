package util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import model.GameSave;
import org.apache.commons.io.FileUtils;
import platform.Platform;

public class GameSaveManager
{
    private final Platform platform;
    private final Gson gson;
    private GameSave gameSave;
    private boolean wasFileCorrupt;

    public GameSaveManager(final Platform platform, final Gson gson)
    {
        this.platform = platform;
        this.gson = gson;
        gameSave();
    }

    public boolean wasFileCorrupt()
    {
        return wasFileCorrupt;
    }

    private File saveFile()
    {
        return FileUtils.getFile(platform.dataDirectory() + "/save.json");
    }

    private GameSave load() throws IOException
    {
        final String json = FileUtils.readFileToString(saveFile(), StandardCharsets.UTF_8);

        GameSave gameSave = gson.fromJson(json, GameSave.class);
        gameSave.setTime();

        return gameSave;
    }

    public GameSave gameSave()
    {
        if (gameSave == null)
        {
            try
            {
                gameSave = load();
            }
            catch (IOException exception)
            {
                gameSave = new GameSave();
            }
            catch (JsonSyntaxException ex)
            {
                try
                {
                    backup();
                }
                catch (IOException ex2)
                {
                    throw new RuntimeException("Failed to backup corrupt save file");
                }

                wasFileCorrupt = true;
                gameSave = new GameSave();
            }
        }

        return gameSave;
    }

    public void save() throws IOException
    {
        if (gameSave.game().user().getUnitNumber() > 10000)
        {
            throw new UnknownError("This is not a valid save. Unit number: "
                + gameSave.game().user().getUnitNumber());
        }
        final File file = saveFile();
        final File destinationDir = FileUtils.getFile(platform.dataDirectory());
        FileUtils.forceMkdir(destinationDir);
        FileUtils.write(file, gson.toJson(gameSave), StandardCharsets.UTF_8);
    }

    public void backup() throws IOException
    {
        final File backupFile = FileUtils.getFile(platform.dataDirectory() + "/save_bkp.json");

        FileUtils.copyFile(saveFile(), backupFile);
    }
}
