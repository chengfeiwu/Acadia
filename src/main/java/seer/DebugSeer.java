package seer;

import java.util.HashMap;
import java.util.Map;
import model.SeerIndex;
import model.Skill;
import model.SkillIndex;
import model.User;

public class DebugSeer extends Seer
{
    private static final Map<Integer, Skill> LEARNABLE_SKILL = new HashMap<>();

    private static final Skill HEAL = new Skill(
        "Debugger Never Die",
        -10000,
        100,
        null,
        0.0,
        0.0,
        0,
        "Unbeatable");

    public DebugSeer(User usr, String nickname)
    {
        super(usr, SeerIndex.BobFlower, nickname, 100, 500);

        if (usr.getUnitNumber() < 10000)
        {
            new UnsupportedOperationException("DebugSeer is only for regular users")
                .printStackTrace();
        }

        addSkill(SkillIndex.Jump.getSkill());
        addSkill(SkillIndex.DieForMe.getSkill());
        addSkill(SkillIndex.LastSurprise.getSkill());
        addSkill(HEAL);
    }
}
