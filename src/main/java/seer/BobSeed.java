package seer;

import java.util.HashMap;
import java.util.Map;
import model.SeerIndex;
import model.Skill;
import model.SkillIndex;
import model.User;

public class BobSeed extends Seer
{
    private static final Map<Integer, Skill> LEARNABLE_SKILL = new HashMap<>();

    static
    {
        LEARNABLE_SKILL.put(0, SkillIndex.Jump.getSkill());
    }

    public BobSeed(User usr, String nickname, int curLevel, int hitPoints)
    {
        super(
            usr,
            SeerIndex.BobSeed,
            nickname.equals("") ? SeerIndex.BobSeed.getCommonName() : nickname,
            curLevel,
            hitPoints);

        addSkill(LEARNABLE_SKILL.get(0));
    }
}
