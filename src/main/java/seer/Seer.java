package seer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import model.Impact;
import model.SeerIndex;
import model.Skill;
import model.User;

public abstract class Seer
{
    private User usr;
    private String nickname;
    private int exp;
    private Impact status;
    private int curLevel;
    private int upgradeLevel;
    private java.util.Map<Integer, Skill> learnableSkill = new HashMap<>(); //Level and Skill
    private java.util.Map<Skill, Integer> currentSkill = new HashMap<>(); //Skill and PP
    private int hitPoints;
    private int maxHitPoints;

    public Seer(
        User usr,
        SeerIndex seer,
        String nickname,
        int curLevel,
        int hitPoints)
    {
        if (curLevel > 100)
        {
            throw new AssertionError();
        }

        this.usr = usr;
        this.nickname = nickname;
        this.hitPoints = hitPoints;
        maxHitPoints = hitPoints;
        this.curLevel = curLevel;
        upgradeLevel = seer.getUpgradeLevel();
        status = null;
    }

    public Seer(User usr,
                SeerIndex seer,
                String nickname, //Nickname of it
                int curLevel,
                int hitPoints,
                int maxHitPoints)
    {
        if (curLevel > 100)
        {
            throw new AssertionError();
        }

        this.usr = usr;
        this.nickname = nickname;
        this.hitPoints = hitPoints;
        this.maxHitPoints = maxHitPoints;
        this.curLevel = curLevel;
        upgradeLevel = seer.getUpgradeLevel();
        status = null;
    }

    public int getHitPointsPercentage()
    {
        return (int)Math.ceil((((double)hitPoints / maxHitPoints) * 100));
    }

    public int getMaxHitPoints()
    {
        return maxHitPoints;
    }

    public void setMaxHitPoints(int hitPoints)
    {
        maxHitPoints = hitPoints;
    }

    public void setLearnableSkill(Map<Integer, Skill> learnableSkill)
    {
        this.learnableSkill = learnableSkill;
    }

    private void levelUp(int inheritExp)
    {
        curLevel++;
        earnExp(inheritExp);
        if (curLevel == upgradeLevel)
        {
            usr.upgradeSeer(this);
        }
        if (learnableSkill.containsKey(curLevel))
        {
            addSkill(learnableSkill.get(curLevel));
        }
    }

    public void earnExp(int exp)
    {
        this.exp += exp;
        int diff = exp - generateLevelUpExp();
        if (diff >= 0)
        {
            levelUp(diff);
        }
    }

    public void receiveAttack(int hpDamage, Impact specialEffect)
    {
        hitPoints -= hpDamage;
        if (specialEffect != null)
        {
            if (status != null && status.isAgainst(specialEffect))
            {
                status = null;
            }
            else
            {
                status = specialEffect;
            }
            return;
        }

        if (status != null)
        {
            status.decreaseRound();
            status = (status.getRoundLeft() == 0) ? null : status;
        }

        if (hitPoints < 0)
        {
            hitPoints = 0;
        }
        else if (hitPoints > maxHitPoints)
        {
            hitPoints = maxHitPoints;
        }
    }

    public void removeStatus()
    {
        status = null;
    }

    public void addSkill(Skill skill)
    {
        if (currentSkill.size() >= 4)
        {
            throw new UnsupportedOperationException("Skill size is beyond four.");
        }
        else
        {
            currentSkill.put(skill, skill.getMaxPP());
        }
    }

    public int getCurrentSkillSize()
    {
        return currentSkill.size();
    }

    public void reduceSkillPP(Skill skill)
    {
        if (!currentSkill.containsKey(skill))
        {
            throw new UnsupportedOperationException(
                "This seer hasn't learned the skill \"" + skill.getName() + "\" yet.");
        }
        currentSkill.put(skill, currentSkill.get(skill) - 1);
    }

    public void swapSkill(Skill removedSkill, Skill replaceSkill)
    {
        if (!currentSkill.containsKey(removedSkill))
        {
            throw new UnsupportedOperationException(
                "The skill " + removedSkill.getName() + " does not yet exist in the list.");
        }
        currentSkill.remove(removedSkill);
        currentSkill.put(replaceSkill, replaceSkill.getMaxPP());
    }

    public int getCurLevel()
    {
        return curLevel;
    }

    public String getNickname()
    {
        return nickname;
    }

    public Impact getStatus()
    {
        return status;
    }

    public boolean canPerformSkill()
    {
        return status == null || !status.isSkipRound();
    }

    public int getHitPoints()
    {
        return hitPoints;
    }

    public boolean containSkill(Skill skill)
    {
        return currentSkill.containsKey(skill);
    }

    public Set<Skill> getCurrentSkill()
    {
        return currentSkill.keySet();
    }

    public int getPP(Skill skill)
    {
        if (!currentSkill.containsKey(skill))
        {
            throw new UnsupportedOperationException(
                "The skill " + skill.getName() + " does not yet exist in the list.");
        }

        return currentSkill.get(skill);
    }

    @Override
    public String toString()
    {
        return "Seer{"
            + "Nickname='" + nickname + '\''
            + ", CurLevel=" + curLevel
            + '}';
    }

    public boolean isSkillCallable(Skill skill)
    {
        if (!currentSkill.containsKey(skill))
        {
            throw new UnsupportedOperationException("Current skill does not exist.");
        }
        else
        {
            return currentSkill.get(skill) > 0;
        }
    }

    public Integer generateLevelUpExp()
    {
        return (int)Math.round(curLevel / 10.0 + 1) * 102;
    }
}
