package battle;

import model.TurnResult;
import seer.Seer;

public class SeerBattle
{
    private final Seer ally;
    private final Seer enemy;
    private Seer firstFainted;

    public SeerBattle(Seer ally, Seer enemy)
    {
        this.ally = ally;
        this.enemy = enemy;
    }

    public Seer getAlly()
    {
        return ally;
    }

    public Seer getEnemy()
    {
        return enemy;
    }

    public boolean isAllyFainted()
    {
        return ally.getHitPoints() <= 0;
    }

    public boolean isEnemyFainted()
    {
        return enemy.getHitPoints() <= 0;
    }

    public TurnResult executeTurn(BattleCommand allyCommand, BattleCommand enemyCommand)
    {
        final var isAllyFirst = isAllyFirst(allyCommand, enemyCommand);
        final BattleCommand first = isAllyFirst ? allyCommand : enemyCommand;
        final BattleCommand second = isAllyFirst ? enemyCommand : allyCommand;

        return new TurnResult(first.execute(), second.execute());
    }

    public static boolean isAllyFirst(BattleCommand allyCommand, BattleCommand enemyCommand)
    {
        return allyCommand.getPriority() >= enemyCommand.getPriority();
    }
}