package battle;

import model.CommandResult;

public interface BattleCommand
{
    int HIGHEST_PRIORITY = 6;
    int MINIMAL_PRIORITY = 0;

    int getPriority();

    boolean isExecutable();

    CommandResult execute();
}
