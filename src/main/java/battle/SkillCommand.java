package battle;

import model.BattleOperation;
import model.CommandResult;
import model.Skill;
import seer.Seer;

public class SkillCommand implements BattleCommand
{
    private Skill skill;
    private Seer ally;
    private Seer enemy;

    public SkillCommand(Skill skill, Seer ally, Seer enemy)
    {
        this.skill = skill;
        this.ally = ally;
        this.enemy = enemy;
    }

    @Override
    public int getPriority()
    {
        return MINIMAL_PRIORITY;
    }

    @Override
    public boolean isExecutable()
    {
        return ally.canPerformSkill() && ally.getHitPoints() > 0;
    }

    @Override
    public CommandResult execute()
    {
        if (!isExecutable())
        {
            return CommandResult.failure(BattleOperation.Fight, ally);
        }
        CommandResult records = skill.performAttack(ally, enemy);
        ally.reduceSkillPP(skill);
        if (ally.getStatus() != null)
        {
            ally.receiveAttack(ally.getStatus().getHealthPointImpact(ally.getMaxHitPoints()), null);
        }
        return records;
    }
}
