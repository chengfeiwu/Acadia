package battle;

import item.Capsule;
import item.Item;
import java.util.Optional;
import model.BattleOperation;
import model.CommandResult;
import model.User;
import seer.Seer;

public class ItemCommand implements BattleCommand
{
    private Optional newSeer;
    private final Seer useOn;
    private final Item item;
    private final User player;

    public ItemCommand(final Seer useOn, final Item item, final User player)
    {
        this.useOn = useOn;
        this.item = item;
        this.player = player;
    }

    @Override
    public int getPriority()
    {
        return HIGHEST_PRIORITY;
    }

    @Override
    public boolean isExecutable()
    {
        return useOn.getHitPoints() > 0;
    }

    @Override
    public CommandResult execute()
    {
        if (!isExecutable())
        {
            return CommandResult.failure(BattleOperation.Bag, useOn).setPlayer(player);
        }
        player.itemUsed(item);
        newSeer = item.useOn(useOn);

        final var result = new CommandResult(BattleOperation.Bag, useOn);

        if (item instanceof Capsule)
        {
            result.setDefendSeer(useOn);
        }
        else
        {
            result.setPerformOrUseOnSeer(useOn);
        }

        result.setMove(item);
        return result;
    }
}
