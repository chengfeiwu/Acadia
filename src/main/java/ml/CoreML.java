package ml;

import battle.BattleCommand;
import battle.SkillCommand;
import java.util.Set;
import model.Skill;
import seer.Seer;

public class CoreML
{
    public static BattleCommand getBattleCommand(Seer attacker, Seer defender)
    {
        Set<Skill> skillList = attacker.getCurrentSkill();

        Skill rtn = null;
        int score = 0;

        for (Skill i : skillList)
        {
            int calcScore = getSimpleSkillScore(i, attacker, defender);
            if (calcScore > score)
            {
                rtn = i;
                score = calcScore;
            }
            else if (calcScore == score && i.getDamage() > rtn.getDamage())
            {
                rtn = i;
            }
        }

        return new SkillCommand(rtn, attacker, defender);
    }

    private static int getSimpleSkillScore(Skill skill, Seer att, Seer def)
    {
        int score = 10;
        int damage = skill.getDamage();
        int maxDamage = skill.getCriticalDamage();

        if (att.getStatus() != null)
        {
            damage += att.getStatus().getAttackPointImpact(damage);
            maxDamage += att.getStatus().getAttackPointImpact(maxDamage);

            int healthPointImpact = att.getStatus().getHealthPointImpact(att.getMaxHitPoints());
            if (att.getHitPoints() - healthPointImpact <= 0)
            {
                score -= 5;
            }
        }

        if (def.getHitPoints() - damage <= 0)
        {
            score += 5;
        }
        else if (def.getHitPoints() - maxDamage <= 0)
        {
            score += 3;
        }

        return score;
    }
}
