package platform;

public class MacPlatform implements Platform
{
    public static final MacPlatform INSTANCE = new MacPlatform();

    private MacPlatform()
    {
    }

    @Override
    public String dataDirectory()
    {
        return System.getProperty("user.home") + "/Library/Application Support/org.projp.Acadia";
    }
}
