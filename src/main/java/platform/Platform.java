package platform;

public interface Platform
{
    static Platform getInstance()
    {
        String operatingSystemName = System.getProperty("os.name").toLowerCase();

        if (operatingSystemName.contains("mac os"))
        {
            return MacPlatform.INSTANCE;
        }

        if (operatingSystemName.contains("windows"))
        {
            return WinPlatform.INSTANCE;
        }

        if (operatingSystemName.contains("linux"))
        {
            return LinuxPlatform.INSTANCE;
        }

        throw new UnsupportedOperationException("Unsupported OS: " + operatingSystemName);
    }

    String dataDirectory();
}
