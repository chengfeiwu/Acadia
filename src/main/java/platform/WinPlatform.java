package platform;

public class WinPlatform implements Platform
{
    public static final WinPlatform INSTANCE = new WinPlatform();

    private WinPlatform()
    {
    }

    @Override
    public String dataDirectory()
    {
        return System.getenv("AppData") + "/org.projp.Acadia";
    }
}
