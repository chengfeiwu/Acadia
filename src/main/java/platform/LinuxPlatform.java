package platform;

public class LinuxPlatform implements Platform
{
    public static final LinuxPlatform INSTANCE = new LinuxPlatform();

    private LinuxPlatform()
    {
    }

    @Override
    public String dataDirectory()
    {
        // See https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
        // for more information on the XDG Base Directory Specification.
        final String xdgConfigHome = System.getenv("XDG_CONFIG_HOME");

        final String configDir = xdgConfigHome == null
            ? System.getProperty("user.home") + "/.config"
            : xdgConfigHome;

        return configDir + "/org.projp.Acadia";
    }
}
