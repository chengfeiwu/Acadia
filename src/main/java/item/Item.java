package item;

import java.util.Optional;
import model.Impact;
import seer.Seer;

public interface Item
{
    Item NORMAL_CAPSULE = new Capsule(
        "Normal Capsule",
        "Normal capsule to catch seer.",
        Capsule.NORMAL_HP_PERCENTAGE);

    Item SMALL_HEAL = new Heal(
        "Small Heal",
        "Heal 30 HP for the seer.",
        Heal.SMALL_HEAL_AMOUNT);

    Item FEAR_RIPPER = new Effect(
        "Fear Ripper",
        "Get rid of the fear status.",
        Impact.Fear);

    Optional<Seer> useOn(Seer seer);

    String getName();

    String getDescription();
}
