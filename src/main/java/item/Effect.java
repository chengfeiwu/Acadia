package item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import model.Impact;
import model.StatModifier;
import seer.Seer;

public class Effect implements Item
{
    private List<Impact> matchedImpact = new ArrayList<>();
    private StatModifier buff = new StatModifier();
    private final String name;
    private final String description;

    Effect(String name, String description, Impact... matchedImpact)
    {
        Collections.addAll(this.matchedImpact, matchedImpact);
        this.description = description;
        this.name = name;
    }

    Effect(String name, String description, StatModifier buff)
    {
        this.buff = buff;
        this.name = name;
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public Optional<Seer> useOn(Seer seer)
    {
        for (Impact i : matchedImpact)
        {
            if (seer.getStatus() == i)
            {
                seer.removeStatus();
                break;
            }
        }
        return Optional.empty();
    }

    @Override
    public String getDescription()
    {
        return description;
    }
}