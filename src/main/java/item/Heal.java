package item;

import java.util.Optional;
import seer.Seer;

public class Heal implements Item
{
    static final int SMALL_HEAL_AMOUNT = 30;
    private final int healAmount;
    private final String description;
    private final String name;

    Heal(String name, String description, int healAmount)
    {
        this.name = name;
        this.description = description;
        this.healAmount = healAmount;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public Optional<Seer> useOn(Seer seer)
    {
        if (seer.getHitPoints() > 0)
        {
            seer.receiveAttack(-healAmount, null);
        }
        return Optional.empty();
    }

    @Override
    public String getDescription()
    {
        return description;
    }
}
