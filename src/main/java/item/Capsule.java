package item;

import java.util.Optional;
import java.util.Random;
import seer.Seer;

public class Capsule implements Item
{
    static final int NORMAL_HP_PERCENTAGE = 15;
    private final String name;
    private final String description;
    private final int hpPercent; //Below this percent, Seers is guaranteed to be captured.

    Capsule(String name, String description, int hpPercent)
    {
        this.name = name;
        this.description = description;
        this.hpPercent = hpPercent;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public Optional<Seer> useOn(Seer seer)
    {
        if (seer.getHitPointsPercentage() <= hpPercent)
        {
            return Optional.of(seer);
        }
        else if (seer.getHitPointsPercentage() - hpPercent <= 30)
        {
            int random = new Random().nextInt(100);
            if (random <= hpPercent)
            {
                return Optional.of(seer);
            }
        }
        return Optional.empty();
    }

    @Override
    public String getDescription()
    {
        return description;
    }
}
