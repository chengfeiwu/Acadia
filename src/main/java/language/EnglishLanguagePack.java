package language;

public enum EnglishLanguagePack implements LanguagePack
{
    TITLE("Project Acadia"),
    START("Press Enter to Start!");

    private String message = "";

    EnglishLanguagePack(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }
}
