package ui.resource;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import model.AirWall;
import model.Building;
import model.Bunker;
import model.Bush;
import model.Map;
import model.NotImplementedException;
import model.Shop;
import model.Terrain;
import model.Tile;
import model.TileObject;

public class MapRepository implements ResourceRepository<Map>
{
    private static TileObject getTileObject(final Color color)
    {
        if (color.equals(Color.valueOf("#008000")))
        {
            return new Bush();
        }
        if (color.equals(Color.valueOf("#ADD8E6")))
        {
            return new AirWall();
        }
        if (color.equals(Color.valueOf("#D3D3D3")))
        {
            return new AirWall(false);
        }
        if (color.equals(Color.valueOf("#A9A9A9")))
        {
            return new Shop();
        }
        if (color.equals(Color.valueOf("#006400")))
        {
            return new Bunker();
        }
        if (color.equals(Color.valueOf("#FFFFFF")))
        {
            return null;
        }

        throw new NotImplementedException();
    }

    private static Terrain getTerrainObject(final Color color)
    {
        if (color.equals(Color.valueOf("#FFFF00")))
        {
            return Terrain.SandRoad;
        }
        if (color.equals(Color.valueOf("#008000")))
        {
            return Terrain.Grass;
        }
        if (color.equals(Color.valueOf("#808080")))
        {
            return Terrain.Debug;
        }

        throw new NotImplementedException();
    }

    private final ResourceRepository<Image> imageRepository;

    public MapRepository(final ResourceRepository<Image> imageRepository)
    {
        this.imageRepository = imageRepository;
    }

    @Override
    public Map get(String filepath)
    {
        // FIXME: Resource file path is a UI concern.
        final var tileObjects = imageRepository.get(String.format("/images/maps/%s.png", filepath));

        final var terrain =
            imageRepository.get(String.format("/images/maps/%s-terrain.png", filepath));

        if (tileObjects.getWidth() != terrain.getWidth()
            || tileObjects.getHeight() != terrain.getHeight())
        {
            throw new IllegalStateException(
                "Tile objects image and terrain image are not the same size");
        }

        final var tileObjectsReader = tileObjects.getPixelReader();
        final var terrainReader = terrain.getPixelReader();
        final var tiles = new Tile[(int)tileObjects.getWidth()][(int)tileObjects.getHeight()];

        for (int y = 0; y < (int)tileObjects.getHeight(); y++)
        {
            for (int x = 0; x < (int)tileObjects.getWidth(); x++)
            {
                final var tileObject = getTileObject(tileObjectsReader.getColor(x, y));

                if (tileObject instanceof Building)
                {
                    final var bottomNeighbor = y + 1 < tileObjects.getHeight()
                        ? getTileObject(tileObjectsReader.getColor(x, y + 1))
                        : null;

                    final var rightNeighbor = x + 1 < tileObjects.getWidth()
                        ? getTileObject(tileObjectsReader.getColor(x + 1, y))
                        : null;

                    // FIXME: Will block off doors of buildings that have another building with the
                    //  same type to their right:
                    //  ,--------, ,--------,
                    //  | BUNKER | | BUNKER |
                    //  |        | |        |
                    //  |     ██ | |     ██ |
                    //  '--------' '--------'
                    //        ^^ This door is treated like a wall

                    final var tileClass = tileObject.getClass();

                    if ((bottomNeighbor == null || bottomNeighbor.getClass() != tileClass)
                        && (rightNeighbor == null || rightNeighbor.getClass() != tileClass))
                    {
                        ((Building)tileObject).setIsWallPiece(false);
                    }
                }

                tiles[x][y] = new Tile(getTerrainObject(terrainReader.getColor(x, y)), tileObject);
            }
        }

        return new Map(tiles);
    }
}
