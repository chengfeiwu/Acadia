package ui.resource;

import javafx.scene.image.Image;

public class ImageRepository implements ResourceRepository<Image>
{
    public Image get(String filePath)
    {
        return new Image(filePath);
    }
}
