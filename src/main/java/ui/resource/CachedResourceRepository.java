package ui.resource;

import java.util.HashMap;
import java.util.Map;

public class CachedResourceRepository<T> implements ResourceRepository<T>
{
    private ResourceRepository<T> innerRepository;
    private Map<String, T> cache = new HashMap<>();

    public CachedResourceRepository(ResourceRepository<T> innerRepository)
    {
        this.innerRepository = innerRepository;
    }

    public T get(String filePath)
    {
        return cache.computeIfAbsent(filePath, innerRepository::get);
    }
}
