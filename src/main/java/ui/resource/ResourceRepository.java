package ui.resource;

public interface ResourceRepository<T>
{
    T get(String filepath);
}
