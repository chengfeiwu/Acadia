package ui.resource;

import javafx.scene.media.Media;

public class MediaRepository implements ResourceRepository<Media>
{
    public Media get(String filePath)
    {
        return new Media(this.getClass().getResource("/" + filePath).toExternalForm());
    }
}