package ui;

import java.util.Scanner;
import model.Impact;

public class BattleDebug
{
    private static Scanner scn = new Scanner(System.in);

    public static void main(String[] args)
    {
        String a = "rm rf";
        for (String x : a.split(" "))
        {
            System.out.println(x);
        }

        //User user_1 = new User("Debug User 1", 10001);
        //User user_2 = new User("Debug User 2", 10002);
        //Seer winner = new DebugSeer(user_1, "William");
        //Seer loser = new BobSeed(user_2, "Hamper", 96, 5000);

        //System.out.println("Debug: Battle System\n");
        //SeerBattle coreBattle = new SeerBattle(winner, loser);
        //System.out.printf("Welcome to the battle between %s and %s!\n",
        //    coreBattle.getAttacker().getNickname(),
        //    coreBattle.getDefender().getNickname());
        //System.out.printf("I bet %s will win.\n\n", winner.getNickname());

        //while (!coreBattle.isDefendFainted() && !coreBattle.isOffendFainted())
        //{
        //    System.out.println("It's " + coreBattle.getAttacker().getNickname() + "'s turn!");
        //    System.out.println(
        //        "Current Status: " + statusToString(coreBattle.getAttacker().getStatus()) + "\n");

        //    Object[] skillSet = new LinkedHashSet<>(coreBattle.getAttacker().getCurrentSkill())
        //        .toArray();

        //    int count = 0;
        //    for (Object i : skillSet)
        //    {
        //        Skill skill = (Skill) i;
        //        System.out.println(count + " " + skill.getName()
        //            + " | PP: " + coreBattle.getAttacker().getPP(skill));
        //        count++;
        //    }
        //    System.out.print("What skill you want to use?\n");

        //    try
        //    {
        //        int select = scn.nextInt();
        //        if (!coreBattle.getAttacker().isSkillCallable((Skill) skillSet[select]))
        //        {
        //            System.out.println("This Skill's PP is out");
        //            continue;
        //        }
        //        coreBattle.attack((Skill) skillSet[select]);
        //    } catch (Exception e)
        //    {
        //        e.printStackTrace();
        //        System.out.println("Bro, pick a valid one...\n");
        //        continue;
        //    }

        //    System.out.println(coreBattle.getDefender().getNickname() + ": "
        //        + coreBattle.getDefender().getHP() + "\n");

        //    coreBattle.swapSeer();
        //}

    }

    public static String statusToString(Impact impact)
    {
        return impact == null ? "No Special Impact" : impact.toString();
    }
}
