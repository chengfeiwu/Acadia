package ui.component;

public interface Component
{
    void goBack();

    boolean isLastPage();
}
