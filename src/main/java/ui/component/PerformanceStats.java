package ui.component;

import com.sun.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import ui.Fxml;

public class PerformanceStats extends VBox
{
    private static final int BYTES_PER_MEGABYTE = 1048576;
    private static final Runtime RUNTIME = Runtime.getRuntime();
    private static final OperatingSystemMXBean OPERATING_SYSTEM =
        ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

    @FXML private Label cpuLabel;
    @FXML private Label jvmMemoryLabel;
    @FXML private Label memoryLabel;

    public PerformanceStats()
    {
        Fxml.load(this);
    }

    public void refresh()
    {
        cpuLabel.setText(String.format("CPU Load: %d%%",
            (int)(OPERATING_SYSTEM.getProcessCpuLoad() * 100)));

        final long memory = RUNTIME.totalMemory() - RUNTIME.freeMemory();
        final int memPercent = (int)(100.0 * memory / RUNTIME.totalMemory());
        jvmMemoryLabel.setText(String.format("JVM Memory Usage: %dMB / %dMB (%d%%)",
            megabytes(memory),
            megabytes(RUNTIME.totalMemory()),
            memPercent));

        final long usedMem = OPERATING_SYSTEM.getTotalPhysicalMemorySize()
            - OPERATING_SYSTEM.getFreePhysicalMemorySize();

        memoryLabel.setText(String.format("Memory Usage: %dMB / %dMB",
            megabytes(usedMem),
            megabytes(OPERATING_SYSTEM.getTotalPhysicalMemorySize())));
    }

    private long megabytes(long bytes)
    {
        return bytes / BYTES_PER_MEGABYTE;
    }
}
