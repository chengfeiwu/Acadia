package ui.component;

import java.util.LinkedHashSet;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import model.Skill;
import seer.Seer;
import ui.Fxml;
import ui.KeyBinder;
import ui.animation.TypewriterTransition;

public class SkillSelectionBox extends VBox implements Component
{
    @FXML private Label infoPP;
    @FXML private Label infoDescription;
    @FXML private Label skill1;
    @FXML private Label skill2;
    @FXML private Label skill3;
    @FXML private Label skill4;

    private Object[] skillSet;
    private int knownMoves;
    private boolean isPause;
    private Runnable event;
    private KeyBinder keyBinder;
    private TypewriterTransition tp;
    private Seer seer;
    private int current = 1;

    public SkillSelectionBox()
    {
        Fxml.load(this);

        this.visibleProperty().addListener(e -> isPause = !this.isVisible());
    }

    public void setSeer(Seer seer)
    {
        this.seer = seer;
        skillSet = new LinkedHashSet<>(seer.getCurrentSkill()).toArray();
        knownMoves = skillSet.length;
        resetSkillLabel();
        for (int i = 0; i < knownMoves; i++)
        {
            setSkillLabel((Skill)skillSet[i], i);
        }
    }

    public void goBack()
    {
    }

    public boolean isLastPage()
    {
        return true;
    }

    private void setSkillLabel(Skill skill, int indicator)
    {
        if (indicator == 0)
        {
            skill1.setText(skill.getName());
        }
        else if (indicator == 1)
        {
            skill2.setText(skill.getName());
        }
        else if (indicator == 2)
        {
            skill3.setText(skill.getName());
        }
        else if (indicator == 3)
        {
            skill4.setText(skill.getName());
        }
        else
        {
            throw new UnsupportedOperationException("Indicator out of bounds: " + indicator);
        }
    }

    public void setMessage(String message)
    {
        setMessage(message, false);
    }

    public void setMessage(String message, boolean isTransitionRequired)
    {
        if (!isTransitionRequired)
        {
            if (tp != null)
            {
                tp.stop();
                tp = null;
            }
            infoDescription.setText(message);
            return;
        }

        if (infoDescription.getText().equals(message)
            || (tp != null && tp.getCurrentTime().lessThan(tp.getTotalDuration())))
        {
            return;
        }
        tp = new TypewriterTransition(infoDescription, message);
        tp.play();
    }

    private void resetSkillLabel()
    {
        skill1.setText("-");
        skill2.setText("-");
        skill3.setText("-");
        skill4.setText("-");
    }

    public void display()
    {
        if (seer == null)
        {
            throw new UnsupportedOperationException("Seer object does not exist.");
        }

        switchLabel(0);

        if (keyBinder == null)
        {
            keyBinder = new KeyBinder(KeyEvent.KEY_PRESSED)
                .bind(new KeyCodeCombination(KeyCode.LEFT), e -> switchLabel(-2))
                .bind(new KeyCodeCombination(KeyCode.RIGHT), e -> switchLabel(2))
                .bind(new KeyCodeCombination(KeyCode.UP), e -> switchLabel(-1))
                .bind(new KeyCodeCombination(KeyCode.DOWN), e -> switchLabel(1))
                .listenTo(this.sceneProperty());
        }
    }

    private void switchLabel(int delta)
    {
        if (isPause && delta != 0)
        {
            return;
        }

        if (inRange(delta) && current + delta <= knownMoves)
        {
            current += delta;
            emphasizeLabel(current);
            Skill curr = (Skill)skillSet[current - 1];
            refreshPP(curr);
            setMessage(curr.getDescription());
        }
    }

    private void refreshPP(Skill curr)
    {
        infoPP.setText("PP: " + seer.getPP(curr) + " / " + curr.getMaxPP());
    }

    private boolean inRange(int delta)
    {
        return current + delta <= 4 && current + delta > 0;
    }

    private void emphasizeLabel(int indicator)
    {
        skill1.getStyleClass().remove("selected");
        skill2.getStyleClass().remove("selected");
        skill3.getStyleClass().remove("selected");
        skill4.getStyleClass().remove("selected");

        if (indicator == 1)
        {
            skill1.getStyleClass().add("selected");
        }
        else if (indicator == 2)
        {
            skill2.getStyleClass().add("selected");
        }
        else if (indicator == 3)
        {
            skill3.getStyleClass().add("selected");
        }
        else if (indicator == 4)
        {
            skill4.getStyleClass().add("selected");
        }
    }

    public void setPause(boolean flag)
    {
        isPause = flag;
    }

    public Skill getCurrentSkill()
    {
        return (Skill)skillSet[current - 1];
    }

    public void setOnFinished(Runnable e)
    {
        this.event = e;
    }
}
