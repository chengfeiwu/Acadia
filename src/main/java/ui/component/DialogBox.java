package ui.component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import ui.Fxml;
import ui.KeyBinder;
import ui.animation.TypewriterTransition;

public class DialogBox extends VBox implements Component
{
    private static final KeyCodeCombination PROGRESS_KEY = new KeyCodeCombination(KeyCode.ENTER);

    @FXML private Label text;

    private final KeyBinder keyBinder;
    private final Queue<String> messages;
    private final Set<Runnable> onFinishedHandlers;
    private boolean isInstantaneous;

    public DialogBox()
    {
        keyBinder = new KeyBinder(KeyEvent.KEY_PRESSED).listenTo(sceneProperty());
        messages = new LinkedList<>();
        onFinishedHandlers = new HashSet<>();

        Fxml.load(this);
    }

    private void showNextMessage()
    {
        final var message = messages.poll();

        if (message == null)
        {
            text.setText("");
            keyBinder.unbind(PROGRESS_KEY);
            onFinishedHandlers.forEach(Runnable::run);
            return;
        }

        final var transition = isInstantaneous
            ? new TypewriterTransition(text, message, 0)
            : new TypewriterTransition(text, message);

        // Make sure the `PROGRESS_KEY` does not have any key bindings,
        // as any previous message would have hooked one up to progress
        // to the current message.
        keyBinder.unbind(PROGRESS_KEY);
        keyBinder.bind(PROGRESS_KEY, e -> transition.jumpTo(transition.getDuration()));

        transition.setOnFinished(e ->
        {
            keyBinder.unbind(PROGRESS_KEY);
            keyBinder.bind(PROGRESS_KEY, e2 -> showNextMessage());
        });

        transition.play();
    }

    public void setInstantaneous()
    {
        isInstantaneous = true;
    }

    public void goBack()
    {
        throw new UnsupportedOperationException("DialogBox don't support this operation.");
    }

    public boolean isLastPage()
    {
        throw new UnsupportedOperationException("DialogBox don't support this operation.");
    }

    public void showMessages(final List<String> messages)
    {
        this.messages.addAll(messages);
        showNextMessage();
    }

    public void addOnFinishedHandler(final Runnable handler)
    {
        onFinishedHandlers.add(handler);
    }

    public void replaceOnFinishedHandler(final Runnable handler)
    {
        onFinishedHandlers.clear();
        onFinishedHandlers.add(handler);
    }
}
