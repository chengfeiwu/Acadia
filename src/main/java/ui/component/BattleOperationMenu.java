package ui.component;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import model.BattleOperation;
import ui.Fxml;
import ui.KeyBinder;

public class BattleOperationMenu extends HBox implements Component
{
    @FXML private Label fight;
    @FXML private Label seer;
    @FXML private Label bag;
    @FXML private Label run;

    private int selectedOperationIndex;

    public BattleOperationMenu()
    {
        selectedOperationIndex = 0;

        new KeyBinder(KeyEvent.KEY_PRESSED)
            .bind(new KeyCodeCombination(KeyCode.RIGHT), e -> moveSelection(2))
            .bind(new KeyCodeCombination(KeyCode.LEFT), e -> moveSelection(-2))
            .bind(new KeyCodeCombination(KeyCode.UP), e -> moveSelection(-1))
            .bind(new KeyCodeCombination(KeyCode.DOWN), e -> moveSelection(1))
            .listenTo(sceneProperty());

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        emphasizeSelectedOperation();
    }

    private void moveSelection(final int delta)
    {
        final int index = selectedOperationIndex + delta;

        if (index < 0 || index >= 4 || !isVisible())
        {
            return;
        }

        selectedOperationIndex = index;
        emphasizeSelectedOperation();
    }

    public void goBack()
    {
        throw new UnsupportedOperationException(
            "BattleOperationMenu don't support this operation.");
    }

    public boolean isLastPage()
    {
        throw new UnsupportedOperationException(
            "BattleOperationMenu don't support this operation.");
    }

    private void emphasizeSelectedOperation()
    {
        fight.getStyleClass().remove("selected");
        seer.getStyleClass().remove("selected");
        bag.getStyleClass().remove("selected");
        run.getStyleClass().remove("selected");

        selectedOperationLabel().getStyleClass().add("selected");
    }

    private Label selectedOperationLabel()
    {
        switch (selectedOperationIndex)
        {
            case 0: return fight;
            case 1: return seer;
            case 2: return bag;
            case 3: return run;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public BattleOperation selectedOperation()
    {
        switch (selectedOperationIndex)
        {
            case 0: return BattleOperation.Fight;
            case 1: return BattleOperation.Seer;
            case 2: return BattleOperation.Bag;
            case 3: return BattleOperation.Run;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
