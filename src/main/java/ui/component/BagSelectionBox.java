package ui.component;

import battle.SeerBattle;
import item.Capsule;
import item.Item;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import model.User;
import ui.Fxml;
import ui.Main;
import ui.resource.ResourceRepository;

public class BagSelectionBox extends VBox implements Component
{
    @FXML private ListView<Item> innerComponent;
    @FXML private DialogBox componentInfo;
    @FXML private ImageView represent;

    private User player;
    private SeerBattle seerBattle;
    private final BooleanProperty isDeciding;
    private Runnable event;
    private ReadOnlyObjectWrapper<Item> decided;
    private final ResourceRepository<Image> ir = Main.IMAGE_REPOSITORY;

    public BagSelectionBox()
    {
        Fxml.load(this);

        isDeciding = new SimpleBooleanProperty(false);
        decided = new ReadOnlyObjectWrapper<>();

        innerComponent.setOnKeyPressed(e ->
        {
            if (e.getCode().equals(KeyCode.ENTER))
            {
                if (!isDeciding.get())
                {
                    isDeciding.setValue(true);
                    List<String> rtn = new ArrayList<>();
                    rtn.add("Press enter again to confirm.");
                    componentInfo.showMessages(rtn);
                }
                else
                {
                    decided.setValue(innerComponent.getFocusModel().getFocusedItem());
                }
            }
        });

        componentInfo.setInstantaneous();

        this.visibleProperty().addListener(e -> decided.set(null));
    }

    @FXML
    private void initialize()
    {
        //FIXME: Current ListCell will have a glitch after clear the item list and add new items.
        innerComponent.setCellFactory((innerComponent) ->
            new ListCell<>()
            {
                @Override
                public void updateItem(Item item, boolean empty)
                {
                    super.updateItem(item, empty);

                    if (item == null)
                    {
                        setText("");
                        return;
                    }

                    setText(item.getName());
                }
            }
        );

        innerComponent.getSelectionModel().selectedItemProperty().addListener((p, o, n) ->
        {
            isDeciding.setValue(false);
            if (n == null)
            {
                return;
            }
            componentInfo.showMessages(getComponentInfo(n));
            represent.setImage(ir.get("/images/item/"
                + n.getClass().getSimpleName().toLowerCase() + ".png"));
        });
    }

    public void goBack()
    {

    }

    public ReadOnlyObjectProperty<Item> getDecided()
    {
        return decided.getReadOnlyProperty();
    }

    public boolean isLastPage()
    {
        return true;
    }

    public void setPlayer(User player)
    {
        this.player = player;
    }

    public void setBattle(SeerBattle seer)
    {
        this.seerBattle = seer;
    }

    public void display(boolean npcExist)
    {
        if (player == null || seerBattle == null)
        {
            throw new UnsupportedOperationException(
                "Player or SeerBattle has not to been initialized in the BagSelectionBox");
        }

        var tmp = player.getItems().keySet();

        innerComponent.getItems().clear();

        for (Item i : tmp)
        {
            if (npcExist && i instanceof Capsule)
            {
                continue;
            }
            innerComponent.getItems().add(i);
        }

        componentInfo.showMessages(
            getComponentInfo(innerComponent.getFocusModel().getFocusedItem()));
        represent.setImage(ir.get("/images/item/"
            +
            innerComponent.getFocusModel().getFocusedItem().getClass().getSimpleName().toLowerCase()
            + ".png"));
    }

    public void setOnFinished(Runnable e)
    {
        this.event = e;
    }

    private List<String> getComponentInfo(Item item)
    {
        List<String> rtn = new ArrayList<>();

        rtn.add(item.getDescription() + " Amount Left: " + player.amountLeft(item));

        return rtn;
    }
}
