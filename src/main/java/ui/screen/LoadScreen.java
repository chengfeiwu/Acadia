package ui.screen;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import ui.Fxml;
import ui.KeyBinder;
import util.GameSaveManager;

public class LoadScreen extends Screen
{
    @FXML
    private Label failed;
    @FXML
    private Label load;
    @FXML
    private Label hint;

    private final GameSaveManager gameSaveManager;
    private boolean success;

    public LoadScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        FadeTransition fadeIn = new FadeTransition(Duration.millis(700), load);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play();

        FadeTransition fadeInText = new FadeTransition(Duration.millis(700), failed);
        fadeInText.setFromValue(0.0);
        fadeInText.setToValue(1.0);

        FadeTransition fadeInHint = new FadeTransition(Duration.millis(700), hint);
        fadeInHint.setFromValue(0.0);
        fadeInHint.setToValue(1.0);

        FadeTransition fadeOutText = new FadeTransition(Duration.millis(700), load);
        fadeOutText.setFromValue(1.0);
        fadeOutText.setToValue(0.0);

        FadeTransition fadeOut = new FadeTransition(Duration.millis(800), this);
        fadeOut.setDelay(Duration.millis(500));
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);

        SequentialTransition seq = new SequentialTransition(fadeOutText, fadeInText, fadeInHint);
        SequentialTransition succLoad = new SequentialTransition(fadeOutText, fadeInText, fadeOut);

        if (gameSaveManager.wasFileCorrupt())
        {
            new KeyBinder(KeyEvent.KEY_PRESSED)
                .bind(new KeyCodeCombination(KeyCode.ENTER), e ->
                {
                    if (success)
                    {
                        return;
                    }
                    FadeTransition faster = new FadeTransition(Duration.millis(200), this);
                    faster.setFromValue(1.0);
                    faster.setToValue(0.0);
                    faster.play();
                    faster.setOnFinished(e2 -> switchTo(new IntroScreen(gameSaveManager)));
                }, true)
                .listenTo(this.sceneProperty());
            seq.play();
            success = false;
        }
        else
        {
            success = true;
            failed.setText("Successfully Loaded Save from " + gameSaveManager.gameSave().date());
            succLoad.play();
            succLoad.setOnFinished(e -> switchTo(new MasterCanvasScreen(gameSaveManager)));
        }
    }
}
