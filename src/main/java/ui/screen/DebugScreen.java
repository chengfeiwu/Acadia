package ui.screen;

import battle.SeerBattle;
import item.Item;
import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import model.Game;
import model.User;
import seer.BobSeed;
import seer.DebugSeer;
import ui.Fxml;
import ui.KeyBinder;
import ui.Main;
import util.GameSaveManager;

public class DebugScreen extends Screen
{
    @FXML private Group debugPane;
    @FXML private VBox debugOptionsPage1;
    @FXML private VBox debugOptionsPage2;
    @FXML private VBox infoPane;
    @FXML private Button switchDebugBuild;
    @FXML private Button exitButton;
    @FXML private Label splashLabel;
    @FXML private Label escapeLabel;
    @FXML private Label operatingSystemLabel;
    @FXML private Label jdkVersionLabel;
    @FXML private Label jdkVendorLabel;
    @FXML private Label unitNumberLabel;
    @FXML private Label locationLabel;

    private final Screen originalScreen;
    private final GameSaveManager gameSaveManager;
    private final Game backup;
    private final BooleanProperty isDebugStageProperty;

    public DebugScreen(final Screen originalScreen, final GameSaveManager gameSaveManager)
    {
        this.originalScreen = originalScreen;
        this.gameSaveManager = gameSaveManager;
        this.backup = gameSaveManager.gameSave().game();

        isDebugStageProperty = new SimpleBooleanProperty(true);

        gameSaveManager.gameSave().setGame(
            new Game(new User("Debug User", 10001)));

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        Main.fadeOutBackgroundMusic();

        final var warningFadeOut = new FadeTransition(Duration.millis(300), splashLabel);
        warningFadeOut.setDelay(Duration.millis(1000));
        warningFadeOut.setFromValue(1.0);
        warningFadeOut.setToValue(0.0);
        warningFadeOut.play();
        warningFadeOut.setOnFinished(event ->
        {
            debugPane.setVisible(true);
            escapeLabel.setVisible(true);

            new KeyBinder(KeyEvent.KEY_PRESSED)
                .bind(new KeyCodeCombination(KeyCode.ESCAPE), e ->
                {
                    exitButton.setVisible(!exitButton.isVisible());
                    debugPane.setVisible(!debugPane.isVisible());
                })
                .bind(new KeyCodeCombination(KeyCode.D), e ->
                {
                    if (!debugOptionsPage1.isVisible())
                    {
                        return;
                    }

                    debugOptionsPage1.setVisible(false);
                    debugOptionsPage2.setVisible(true);
                })
                .bind(new KeyCodeCombination(KeyCode.A), e ->
                {
                    if (!debugOptionsPage2.isVisible())
                    {
                        return;
                    }
                    debugOptionsPage1.setVisible(true);
                    debugOptionsPage2.setVisible(false);
                })
                .listenTo(sceneProperty());
        });

        switchDebugBuild.textProperty().bind(Bindings.createStringBinding(() ->
            isDebugStageProperty.get() ? "Debug Stage" : "Production Stage", isDebugStageProperty));

        escapeLabel.textProperty().bind(Bindings.createStringBinding(() ->
            isDebugStageProperty.get()
                ? "This is a Debug Stage. All debug features are available."
                : "This is a Production Stage. Certain screens have been disabled.",
            isDebugStageProperty));
    }

    public BooleanProperty isDebugStageProperty()
    {
        return isDebugStageProperty;
    }

    public boolean getIsDebugStage()
    {
        return isDebugStageProperty.get();
    }

    @FXML
    void canvasScreen()
    {
        switchTo(new MasterCanvasScreen(gameSaveManager, isDebugStageProperty.get()));
    }

    @FXML
    void splashScreen()
    {
        switchTo(new SplashScreen(gameSaveManager));
    }

    @FXML
    void homeScreen()
    {
        switchTo(new HomeScreen(gameSaveManager));
    }

    @FXML
    void loadScreen()
    {
        switchTo(new LoadScreen(gameSaveManager));
    }

    @FXML
    void preSettingUpScreen()
    {
        switchTo(new PreSettingUpScreen(gameSaveManager));
    }

    @FXML
    void nameInputScreen()
    {
        switchTo(new NameInputScreen(gameSaveManager));
    }

    @FXML
    void postSettingUpScreen()
    {
        switchTo(new PostSettingUpScreen(gameSaveManager));
    }

    @FXML
    void systemSetupScreen()
    {
        switchTo(new SystemSetupScreen(gameSaveManager));
    }

    @FXML
    void saveStateScreen()
    {
        switchTo(new SaveStateScreen(gameSaveManager));
    }

    @FXML
    void showInfo()
    {
        debugOptionsPage2.setVisible(false);
        infoPane.setVisible(true);

        operatingSystemLabel.setText("Operating System: " + System.getProperty("os.name"));
        jdkVersionLabel.setText("JDK Version: " + System.getProperty("java.version"));
        jdkVendorLabel.setText("Vendor: " + System.getProperty("java.vendor"));

        final var game = gameSaveManager.gameSave().game();
        unitNumberLabel.setText("Unit Number: " + game.user().getUnitNumber());
        locationLabel.setText("Player Location: " + game.playerPosition());
    }

    @FXML
    void hideInfo()
    {
        infoPane.setVisible(false);
        debugOptionsPage2.setVisible(true);
    }

    @FXML
    void switchDebug()
    {
        isDebugStageProperty.set(!isDebugStageProperty.get());

        gameSaveManager.gameSave().setGame(isDebugStageProperty.get()
            ? new Game(new User("Debug User", 10001))
            : backup);
    }

    @FXML
    void battleScreen()
    {
        final var winner = new User("Debug User 1", 10001);
        final var winnerSeer = new DebugSeer(winner, "Schafer");

        final var loser = new User("Debug User 2", 10002);
        final var loserSeer = new BobSeed(loser, "Hamper", 96, 1000);

        if (isDebugStageProperty.get())
        {
            gameSaveManager.gameSave().game().user().addItems(
                Item.FEAR_RIPPER,
                Item.NORMAL_CAPSULE,
                Item.SMALL_HEAL);
        }

        final var battle = new SeerBattle(winnerSeer, loserSeer);
        switchTo(new BattleScreen(gameSaveManager, battle, this));
    }

    @FXML
    void exit()
    {
        gameSaveManager.gameSave().setGame(backup);
        switchTo(originalScreen);
    }
}
