package ui.screen;

import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.fxml.FXML;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import ui.Fxml;
import ui.KeyBinder;
import util.GameSaveManager;

public class CopyrightScreen extends Screen
{
    @FXML private VBox foreground;
    @FXML private Rectangle background;

    private final GameSaveManager gameSaveManager;

    public CopyrightScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;
        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        background.widthProperty().bind(this.widthProperty());
        background.heightProperty().bind(this.heightProperty());

        FillTransition backgroundFillIn = new FillTransition(
            Duration.millis(300),
            background,
            Color.WHITE,
            Color.BLACK);

        FadeTransition foregroundFadeIn = new FadeTransition(Duration.millis(200), foreground);
        foregroundFadeIn.setToValue(1.0);

        PauseTransition pause = new PauseTransition(Duration.millis(2500));

        FadeTransition foregroundFadeOut = new FadeTransition(Duration.millis(400), foreground);
        foregroundFadeOut.setToValue(0.0);

        FillTransition backgroundFillOut = new FillTransition(
            Duration.millis(500),
            background,
            Color.BLACK,
            Color.WHITE);

        SequentialTransition sequence = new SequentialTransition(
            backgroundFillIn,
            foregroundFadeIn,
            pause,
            foregroundFadeOut,
            backgroundFillOut);

        sequence.setOnFinished(e -> loadSplashScreen());
        sequence.play();

        Duration outStartTime = backgroundFillIn.getDuration()
            .add(foregroundFadeIn.getDuration())
            .add(pause.getDuration());

        new KeyBinder(KeyEvent.KEY_PRESSED).bindAnyKey(e ->
        {
            if (sequence.getCurrentTime().lessThan(outStartTime))
            {
                sequence.jumpTo(outStartTime);
            }

            sequence.setRate(2);
        }, true)
            .listenTo(this.sceneProperty());
    }

    private void loadSplashScreen()
    {
        switchTo(new SplashScreen(gameSaveManager));
    }
}
