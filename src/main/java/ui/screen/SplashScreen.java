package ui.screen;

import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import ui.Fxml;
import ui.KeyBinder;
import ui.Main;
import util.GameSaveManager;

public class SplashScreen extends Screen
{
    @FXML private Label version;
    @FXML private Label start;

    private final GameSaveManager gameSaveManager;

    public SplashScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        new KeyBinder(KeyEvent.KEY_PRESSED)
            .bind(new KeyCodeCombination(KeyCode.ENTER), e -> continueAction(), true)
            .bind(new KeyCodeCombination(KeyCode.ESCAPE, KeyCombination.CONTROL_DOWN), e ->
            {
                final var fadeOut = new FadeTransition(Duration.millis(500), this);
                fadeOut.setFromValue(1.0);
                fadeOut.setToValue(0.0);
                fadeOut.play();
                fadeOut.setOnFinished(x -> Platform.exit());
            }, true)
            .listenTo(this.sceneProperty());

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        Main.backgroundMusic.setVolume(gameSaveManager.gameSave().getSettings().getVolume());
        Main.backgroundMusic.play();

        final var fadeIn = new FadeTransition(Duration.millis(1000), this);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play();

        if (gameSaveManager.wasFileCorrupt())
        {
            start.setText("Due to a loading issue, the current save file has been reset.");
            start.getStyleClass().add("alert");

            final var pause = new PauseTransition(Duration.millis(5000));

            final var switchMessage = new FadeTransition(Duration.millis(600), start);
            switchMessage.setFromValue(1.0);
            switchMessage.setToValue(0.0);
            switchMessage.setOnFinished(e ->
            {
                start.setText("Press Enter to Start!");
                start.getStyleClass().remove("alert");

                version.setText("Previous file is still available. Press ENTER to learn more.");
                version.getStyleClass().add("alert");
            });

            final var showMessage = new FadeTransition(Duration.millis(600), start);
            showMessage.setFromValue(0.0);
            showMessage.setToValue(1.0);

            new SequentialTransition(pause, switchMessage, showMessage).play();
        }
    }

    @FXML
    private void continueAction()
    {
        final var fadeIn = new FadeTransition(Duration.millis(250), this);
        fadeIn.setFromValue(1.0);
        fadeIn.setToValue(0.0);
        fadeIn.play();
        fadeIn.setOnFinished(e -> switchTo(new HomeScreen(gameSaveManager)));
    }
}
