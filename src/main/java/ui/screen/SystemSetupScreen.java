package ui.screen;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.Duration;
import ui.Fxml;
import ui.animation.TypewriterTransition;
import util.GameSaveManager;

public class SystemSetupScreen extends Screen
{
    @FXML
    private Label one;
    @FXML
    private Label two;
    @FXML
    private Label three;
    @FXML
    private Label four;
    @FXML
    private Label five;
    @FXML
    private Label six;
    @FXML
    private Label seven;
    @FXML
    private Label eight;
    @FXML
    private Label nine;

    private GameSaveManager gameSaveManager;
    private boolean finished = true;

    public SystemSetupScreen(GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        FadeTransition background = new FadeTransition(Duration.millis(600), this);
        background.setFromValue(0.0);
        background.setToValue(1.0);

        FadeTransition fadeOut = new FadeTransition(Duration.millis(1000), this);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setOnFinished(e -> switchTo(new MasterCanvasScreen(gameSaveManager)));

        SequentialTransition main = new SequentialTransition(
            background,
            new TypewriterTransition(one, "Initializing Boot Image", 30),
            new TypewriterTransition(two, "Memory Unit: Green", 30),
            new TypewriterTransition(three, "Loading Geographic Data", 30),
            new TypewriterTransition(four, "Black Box State: Normal", 30),
            new TypewriterTransition(five, "Activating MEE", 15),
            new TypewriterTransition(six, "Open Connection With Remote Host", 13),
            new TypewriterTransition(seven, "Checking CA", 15),
            new TypewriterTransition(eight, "Activating Environmental Sensors", 13),
            new TypewriterTransition(nine, "All Systems Green", 15));

        main.setOnFinished(e -> fadeOut.play());
        main.play();
    }
}