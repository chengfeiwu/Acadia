package ui.screen;

import java.util.List;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.util.Duration;
import ui.Fxml;
import ui.component.DialogBox;
import util.GameSaveManager;

public class PreSettingUpScreen extends Screen
{
    @FXML private DialogBox dialog;

    private final GameSaveManager gameSaveManager;

    public PreSettingUpScreen(GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        FadeTransition fadeOut = new FadeTransition(Duration.millis(500), this);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);

        final var messages = List.of(
            "<This system has an auto save feature built in. However, it will not be triggered "
                + "until the system shuts down or detects a hardware malfunction. Please be aware "
                + "that any decision you make might change the e...en...>",
            "<(Finished in 876623 Hours, 16 Minutes, 32 Seconds) System recovery has finished, 96% "
                + "of data successfully recovered...>",
            "<Commander 105 Signing off...>",
            "<Self Control Engaged...>",
            "<Visual Component Currently Down...>",
            "???: You are finally awake, our hero.",
            "<Memory Leak>: Wh...Where am I...",
            "???: Listen, I know you feel very strange right now.",
            "???: You have been put into sleep for one hundred years.",
            "???: I am Dr. DeNero, chief scientist at Bunker.",
            "DeNero: Now tell me. What is your name..."
        );

        dialog.prefWidthProperty().bind(this.widthProperty());
        dialog.showMessages(messages);
        dialog.addOnFinishedHandler(fadeOut::play);
        fadeOut.setOnFinished(event -> switchTo(new NameInputScreen(gameSaveManager)));
    }
}
