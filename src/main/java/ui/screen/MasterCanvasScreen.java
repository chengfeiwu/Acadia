package ui.screen;

import java.io.IOException;
import java.util.List;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import model.Direction;
import model.Game;
import model.Vector2D;
import ui.Fxml;
import ui.KeyBinder;
import ui.Main;
import ui.animation.CanvasOperator;
import ui.animation.CharacterMovingTransition;
import ui.component.DialogBox;
import ui.component.PerformanceStats;
import util.GameSaveManager;

public class MasterCanvasScreen extends Screen
{
    private static final int TIME_TO_TURN = 200; // milliseconds

    @FXML private Canvas backgroundCanvas;
    @FXML private Canvas characterCanvas;
    @FXML private PerformanceStats performanceStats;
    @FXML private VBox statsOverlay;
    @FXML private Label playerLocationLabel;
    @FXML private Label message;
    @FXML private DialogBox dialog;

    private final GameSaveManager gameSaveManager;
    private final boolean isDebug;
    private final Timeline statsOverlayTimeline;
    private boolean isPlayerMoving;
    private boolean isLeftKeyPressed;
    private boolean isRightKeyPressed;
    private boolean isUpKeyPressed;
    private boolean isDownKeyPressed;
    private double movementCooldown;

    // FIXME: Direction should be modeled under the `User` class, not here
    private Direction currentDirection;

    public MasterCanvasScreen(final GameSaveManager gameSaveManager)
    {
        this(gameSaveManager, false);
    }

    public MasterCanvasScreen(final GameSaveManager gameSaveManager, final boolean isDebug)
    {
        this.gameSaveManager = gameSaveManager;
        this.isDebug = isDebug;

        statsOverlayTimeline =
            new Timeline(new KeyFrame(Duration.millis(100), e -> refreshStatsOverlay()));

        statsOverlayTimeline.setCycleCount(Animation.INDEFINITE);
        currentDirection = Direction.Up;

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        final Game game = gameSaveManager.gameSave().game();

        game.setMap(Main.MAP_REPOSITORY.get(isDebug ? "test" : "production"));

        if (!game.isValid())
        {
            throw new IllegalStateException("Game object is not complete.");
        }

        backgroundCanvas.heightProperty().bind(heightProperty());
        backgroundCanvas.widthProperty().bind(widthProperty());

        backgroundCanvas.heightProperty().addListener(w ->
            CanvasOperator.mapCanvas(game, backgroundCanvas));

        characterCanvas.heightProperty().bind(heightProperty());
        characterCanvas.widthProperty().bind(widthProperty());
        characterCanvas.heightProperty().addListener(w ->
            CanvasOperator.drawCharacter(currentDirection, characterCanvas, game));

        new KeyBinder(KeyEvent.KEY_PRESSED)
            .bind(new KeyCodeCombination(KeyCode.LEFT), e -> isLeftKeyPressed = true)
            .bind(new KeyCodeCombination(KeyCode.RIGHT), e -> isRightKeyPressed = true)
            .bind(new KeyCodeCombination(KeyCode.UP), e -> isUpKeyPressed = true)
            .bind(new KeyCodeCombination(KeyCode.DOWN), e -> isDownKeyPressed = true)
            .bind(new KeyCodeCombination(KeyCode.S, KeyCombination.SHIFT_ANY), e ->
            {
                //development stage
                if (game.user().getUnitNumber() <= 10000)
                {
                    try
                    {
                        gameSaveManager.save();
                    }
                    catch (IOException ex)
                    {
                        ex.printStackTrace();
                    }
                }
                else
                {
                    displayMessage(
                        "This method is for normal testing, debug build is currently not "
                        + "supported.",
                        true);
                }
            })
            .bind(new KeyCodeCombination(KeyCode.I), e -> toggleStatsOverlay())
            .listenTo(this.sceneProperty());

        new KeyBinder(KeyEvent.KEY_RELEASED)
            .bind(new KeyCodeCombination(KeyCode.LEFT), e -> isLeftKeyPressed = false)
            .bind(new KeyCodeCombination(KeyCode.RIGHT), e -> isRightKeyPressed = false)
            .bind(new KeyCodeCombination(KeyCode.UP), e -> isUpKeyPressed = false)
            .bind(new KeyCodeCombination(KeyCode.DOWN), e -> isDownKeyPressed = false)
            .listenTo(this.sceneProperty());

        // FIXME: Pull this out of initialize and into someplace more appropriate (make a class?)
        new AnimationTimer()
        {
            private long lastHandleTime;

            @Override
            public void start()
            {
                super.start();
                lastHandleTime = System.nanoTime();
            }

            @Override
            public void handle(final long now)
            {
                final double timeDelta = (now - lastHandleTime) / 1_000_000_000.0;
                lastHandleTime = now;

                handleInput(timeDelta);
            }
        }.start();

        showStatsOverlay(isDebug);

        dialog.addOnFinishedHandler(() -> dialog.setVisible(false));
    }

    private void displayMessage(final String text, final boolean isImportant)
    {
        message.setText(text);
        message.setStyle(String.format("-fx-text-fill: #%s;", isImportant ? "ff225c" : "000000"));
        message.setVisible(true);

        PauseTransition pause = new PauseTransition(Duration.millis(text.length() * 100));
        pause.setOnFinished(e ->
        {
            message.setVisible(false);
            message.setText("");
        });
        pause.play();
    }

    private void toggleStatsOverlay()
    {
        showStatsOverlay(!statsOverlay.isVisible());
    }

    private void showStatsOverlay(final boolean isShown)
    {
        statsOverlay.setVisible(isShown);

        if (isShown)
        {
            statsOverlayTimeline.play();
        }
        else
        {
            statsOverlayTimeline.stop();
        }
    }

    private void refreshStatsOverlay()
    {
        performanceStats.refresh();

        playerLocationLabel.setText(String.format("Player Location: %s",
            gameSaveManager.gameSave().game().playerPosition()));
    }

    public void handleInput(final double timeDelta)
    {
        if (movementCooldown > 0)
        {
            movementCooldown = Math.max(0, movementCooldown - timeDelta * 1000.0);
        }

        final var direction = isLeftKeyPressed && !isRightKeyPressed ? Direction.Left
            : isRightKeyPressed && !isLeftKeyPressed ? Direction.Right
            : isUpKeyPressed && !isDownKeyPressed ? Direction.Up
            : isDownKeyPressed && !isUpKeyPressed ? Direction.Down
            : null;

        if (direction == null || isPlayerMoving || dialog.isVisible())
        {
            return;
        }

        final var game = gameSaveManager.gameSave().game();

        if (!currentDirection.equals(direction))
        {
            currentDirection = direction;
            movementCooldown = TIME_TO_TURN;
            CanvasOperator.drawCharacter(direction, characterCanvas, game);
            return;
        }

        if (movementCooldown > 0 || !game.canPlayerMove(direction))
        {
            return;
        }

        final var movement = direction.toVector2D();
        final boolean willViewChange = CanvasOperator.willViewChange(game, movement);

        final var characterMovingTransition =
            new CharacterMovingTransition(characterCanvas, direction, game.playerPosition());

        characterMovingTransition.setOnFinished(e ->
        {
            isPlayerMoving = false;
            game.movePlayer(movement);
            showDialogForPosition(game.playerPosition());
            autoSaveTrigger(game.playerPosition());

            if (willViewChange)
            {
                CanvasOperator.mapCanvas(game, backgroundCanvas);
            }

            final var tile = game.map().tileAt(game.playerPosition());
            if (tile.hasTileObject() && tile.tileObject().isInteractive())
            {
                CanvasOperator.interactMap(game, backgroundCanvas);

                // FIXME: This is not the responsibility of the UI -
                //  `game.movePlayer` should do this.
                tile.tileObject().interact(game.user());
            }

            CanvasOperator.drawCharacter(currentDirection, characterCanvas, game);
        });

        characterMovingTransition.play();
        isPlayerMoving = true;
    }

    public void showDialogForPosition(final Vector2D position)
    {
        // FIXME: Dialog triggers should be stored in the Map, not hard-coded in
        // the MasterCanvasScreen.
        if (position.equals(new Vector2D(2, 0)))
        {
            showDialog(List.of(
                "SysCore: ME System Checking...",
                "SysCore: Passed."
            ));
        }
        else if (position.equals(new Vector2D(12, 4)))
        {
            showDialog(List.of(
                "SysCore: Vision Components Checking...",
                "SysCore: Passed."
            ));
        }
        else if (position.equals(new Vector2D(13, 5)))
        {
            showDialog(List.of(
                "SysCore: Self Check Completed.",
                "SysCore: Syncing Current Status with Bunker...",
                "Authorize Code: 416C6C79E38292E5A4A7E5A5BDE3818DE381A0",
                "<Everything that lives is designed to end. We are perpetually trapped in a "
                    + "never-ending spiral of life and death. Is this a curse? Or some kind of "
                    + "punishment? I often think about the god who blessed us with this cryptic "
                    + "puzzle...and wonder if we'll ever get the chance to kill him...>"
            ));
        }
        else if (position.equals(new Vector2D(14, 5)))
        {
            final Game game = gameSaveManager.gameSave().game();

            if (!game.canTriggerConversation("introConversation"))
            {
                String name = game.user().getNickname();
                game.triggerConversation("introConversation");
                showDialog(List.of(
                    "Where...",
                    "Where is this place...",
                    "???: " + name + ", Come in.",
                    name + ": Go ahead.",
                    "???: I am your operator. Unit number 192. Here are some information you need "
                        + "to know, I have just sent them.",
                    "New Message Received.",
                    "Code name: 9S  Unit number: " + (game.user().getUnitNumber() > 10000
                        ? game.user().getUnitNumber() + " (SU Privilege)"
                        : game.user().getUnitNumber()),
                    name + ": I just read them.",
                    "Operator 192: Great! I will call your code name next time.",
                    "Operator 192: Now, try to move yourself a little bit, I will explain more "
                        + "things to you later."
                ));
            }
        }
    }

    public void autoSaveTrigger(final Vector2D position)
    {
        if (gameSaveManager.gameSave().game().user().getUnitNumber() >= 10000)
        {
            return;
        }

        // FIXME: Auto-save trigger should be specified in map,
        // not hard-coded in the MasterCanvasScreen.
        if (position.equals(new Vector2D(14, 5)))
        {
            try
            {
                gameSaveManager.save();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void showDialog(final List<String> message)
    {
        dialog.setVisible(true);
        dialog.showMessages(message);
    }
}
