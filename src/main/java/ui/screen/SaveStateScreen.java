package ui.screen;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.Duration;
import ui.Fxml;
import util.GameSaveManager;

public class SaveStateScreen extends Screen
{
    @FXML
    private Label info;

    private GameSaveManager gameSaveManager;

    public SaveStateScreen(GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        if (gameSaveManager.gameSave().game().user() != null)
        {
            info.setText("Finishing Auto Save™ Process...");
            // TODO: Save All User Info
        }

        FadeTransition init = new FadeTransition(Duration.millis(500), this);
        init.setFromValue(0);
        init.setToValue(1);
        FadeTransition end = new FadeTransition(Duration.millis(700), this);
        end.setDelay(Duration.millis(200));
        end.setFromValue(1);
        end.setToValue(0);

        init.play();
        init.setOnFinished(e -> end.play());
        end.setOnFinished(e -> Platform.exit());
    }

    private boolean isValidScreen(String name)
    {
        return !name.equals("CopyrightScreen")
            && !name.equals("SplashScreen")
            && !name.equals("HomeScreen")
            && !name.equals("LoadScreen")
            && !name.equals("PreSettingUpScreen")
            && !name.equals("IntroScreen")
            && !name.equals("NameInputScreen")
            && !name.equals("DebugScreen");
    }
}
