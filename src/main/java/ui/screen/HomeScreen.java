package ui.screen;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import model.Game;
import model.GameSave;
import ui.Fxml;
import ui.KeyBinder;
import ui.Main;
import ui.animation.TypewriterTransition;
import ui.resource.ResourceRepository;
import util.GameSaveManager;

public class HomeScreen extends Screen
{
    @FXML
    private Label ng;
    @FXML
    private Label lg;
    @FXML
    private Label egg;
    @FXML
    private Label diag;
    @FXML
    private VBox confirm;
    @FXML
    private VBox failMessage;
    private final GameSaveManager gameSaveManager;
    private int functionButton;
    private ResourceRepository<Image> image = Main.IMAGE_REPOSITORY;
    private ResourceRepository<Media> media = Main.MEDIA_REPOSITORY;
    private Media selectSoundEffect = media.get("media/selc.mp3");
    private MediaPlayer mp = new MediaPlayer(selectSoundEffect);
    private boolean newGameConflict;
    private boolean isWaiting;
    private boolean fail;

    public HomeScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;
        fail = gameSaveManager.wasFileCorrupt();

        double masterVolume = gameSaveManager.gameSave().getSettings().getVolume();

        mp.setVolume(masterVolume >= 0.2 ? 0.2 : masterVolume);

        GameSave check = gameSaveManager.gameSave();
        newGameConflict = check.game().user() != null;

        new KeyBinder(KeyEvent.KEY_PRESSED)
            .bind(new KeyCodeCombination(KeyCode.ENTER), e ->
            {
                if (fail)
                {
                    failMessage.setVisible(false);
                    isWaiting = false;
                    fail = false;
                    return;
                }

                mp.stop();
                mp.play();
                if (functionButton == 0)
                {
                    // This is a new Game, so create a new Game instance
                    // Add key binding to the Window instance that detects exit key combo
                    // - Event handler will pass that Game instance into the constructor for a
                    //   new SaveStateScreen.
                    if (newGameConflict)
                    {
                        diag.setText("");

                        TypewriterTransition tt = new TypewriterTransition(
                            diag,
                            "There is a saved game already existed...",
                            30);

                        tt.play();
                        confirm.setVisible(true);
                        isWaiting = true;
                        return;
                    }

                    newGame();
                }
                else if (functionButton == 1)
                {
                    load();
                }
                else
                {
                    throw new UnsupportedOperationException("Indicator out of range.");
                }
            })
            .bind(new KeyCodeCombination(KeyCode.UP), e ->
            {
                if (isWaiting)
                {
                    return;
                }

                if (functionButton == 1)
                {
                    ng.setText("* New Game");
                    lg.setText("  Load Game");
                    functionButton = 0;
                    mp.stop();
                    mp.play();
                }
            })
            .bind(new KeyCodeCombination(KeyCode.DOWN), e ->
            {
                if (isWaiting)
                {
                    return;
                }

                if (functionButton == 0)
                {
                    ng.setText("  New Game");
                    lg.setText("* Load Game");
                    functionButton = 1;
                    mp.stop();
                    mp.play();
                }
            })
            .bind(new KeyCodeCombination(KeyCode.LEFT), event -> egg.setOpacity(1.0), true)
            .bind(new KeyCodeCombination(KeyCode.ESCAPE), e ->
            {
                if (isWaiting)
                {
                    load();
                    return;
                }
                switchTo(new SplashScreen(gameSaveManager));
            }, true)
            .bind(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN), event ->
            {
                if (isWaiting & confirm.isVisible())
                {
                    newGame();
                }
            }, true)
            .listenTo(this.sceneProperty());

        new KeyBinder(KeyEvent.KEY_RELEASED)
            .bind(new KeyCodeCombination(KeyCode.LEFT), e -> egg.setOpacity(0.0), true)
            .listenTo(this.sceneProperty());

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        if (fail)
        {
            isWaiting = true;
            failMessage.setVisible(true);
        }
    }

    private void load()
    {
        FadeTransition fadeIn = new FadeTransition(Duration.millis(240), this);
        fadeIn.setFromValue(1.0);
        fadeIn.setToValue(0.0);
        fadeIn.play();
        Main.fadeOutBackgroundMusic();
        fadeIn.setOnFinished((ActionEvent event) -> switchTo(new LoadScreen(gameSaveManager)));
    }

    private void newGame()
    {
        Game game = new Game();
        FadeTransition fadeIn = new FadeTransition(Duration.millis(240), this);
        fadeIn.setFromValue(1.0);
        fadeIn.setToValue(0.0);
        fadeIn.play();
        Main.fadeOutBackgroundMusic();
        fadeIn.setOnFinished((ActionEvent event) -> switchTo(new IntroScreen(gameSaveManager)));
    }
}
