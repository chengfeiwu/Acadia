package ui.screen;

import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.Duration;
import ui.Fxml;
import util.GameSaveManager;

public class IntroScreen extends Screen
{
    @FXML private Label pre;

    private final GameSaveManager gameSaveManager;

    public IntroScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;
        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        FadeTransition fadeIn = new FadeTransition(Duration.millis(600), pre);
        fadeIn.setDelay(Duration.millis(500));
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);

        FadeTransition fadeOutText = new FadeTransition(Duration.millis(700), pre);
        fadeOutText.setFromValue(1.0);
        fadeOutText.setToValue(0.0);

        FadeTransition fadeInText = new FadeTransition(Duration.millis(700), pre);
        fadeInText.setFromValue(0.0);
        fadeInText.setToValue(1.0);

        FadeTransition fadeOut = new FadeTransition(Duration.millis(600), this);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);

        PauseTransition oneSec = new PauseTransition(Duration.millis(1600));

        fadeIn.play();
        fadeIn.setOnFinished(e -> oneSec.play());
        oneSec.setOnFinished(e -> fadeOutText.play());
        fadeOutText.setOnFinished(e ->
        {
            pre.setText("A Project Acadia Production.");
            fadeInText.play();
        });

        PauseTransition anotherOneSec = new PauseTransition(Duration.millis(1600));

        fadeInText.setOnFinished(e -> anotherOneSec.play());
        anotherOneSec.setOnFinished(e -> fadeOut.play());
        fadeOut.setOnFinished(e -> switchTo(new PreSettingUpScreen(gameSaveManager)));
    }
}
