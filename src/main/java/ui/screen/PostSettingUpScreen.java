package ui.screen;

import java.util.List;
import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.fxml.FXML;
import javafx.util.Duration;
import ui.Fxml;
import ui.component.DialogBox;
import util.GameSaveManager;

public class PostSettingUpScreen extends Screen
{
    @FXML private DialogBox dialog;

    private GameSaveManager gameSaveManager;

    public PostSettingUpScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        FadeTransition one = new FadeTransition(Duration.millis(50), this);
        one.setFromValue(1.0);
        one.setToValue(0.0);
        FadeTransition two = new FadeTransition(Duration.millis(60), this);
        two.setFromValue(0.0);
        two.setToValue(1.0);
        FadeTransition three = new FadeTransition(Duration.millis(70), this);
        three.setFromValue(1.0);
        three.setToValue(0.0);
        FadeTransition four = new FadeTransition(Duration.millis(60), this);
        four.setFromValue(0.0);
        four.setToValue(1.0);

        String name = gameSaveManager.gameSave().game().user().getNickname();
        FadeTransition fadeOut = new FadeTransition(Duration.millis(80), this);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);

        final var messages = List.of(
            String.format("%s: Dr. DeNero, I am %s.", name, name),
            "DeNero: Oh my god! I can't believe you still remember your name!",
            "DeNero: By the way, you can just call me John.",
            "John: Let the system take a second to load up since you just woke up.",
            String.format("%s: System?", name),
            "John: Yes, your are an android. Do you still remember..."
        );

        SequentialTransition splash = new SequentialTransition(one, two, three, four);

        dialog.prefWidthProperty().bind(this.widthProperty());
        dialog.showMessages(messages);
        dialog.addOnFinishedHandler(splash::play);
        splash.setOnFinished(e -> fadeOut.play());
        fadeOut.setOnFinished(e ->
            switchTo(new SystemSetupScreen(gameSaveManager)));
    }
}
