package ui.screen;

import battle.BattleCommand;
import battle.ItemCommand;
import battle.SeerBattle;
import battle.SkillCommand;
import item.Capsule;
import item.Item;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import ml.CoreML;
import model.BattleOperation;
import model.CommandResult;
import model.Skill;
import model.TurnResult;
import model.User;
import seer.Seer;
import ui.Fxml;
import ui.KeyBinder;
import ui.Main;
import ui.component.BagSelectionBox;
import ui.component.BattleOperationMenu;
import ui.component.Component;
import ui.component.DialogBox;
import ui.component.SkillSelectionBox;
import ui.resource.ResourceRepository;
import util.GameSaveManager;

class BattleScreen extends Screen
{
    @FXML private ImageView enemy;
    @FXML private ImageView ally;
    @FXML private Label enemyName;
    @FXML private Label enemyHp;
    @FXML private Label enemyLevel;
    @FXML private Label allyName;
    @FXML private Label allyHp;
    @FXML private Label allyLevel;
    @FXML private SkillSelectionBox skillPane;
    @FXML private BattleOperationMenu menuPane;
    @FXML private BagSelectionBox bagPane;
    @FXML private DialogBox dialogPane;

    private Thread t;
    private User npc;
    private final GameSaveManager gsm;
    private FadeTransition ft;
    private final SeerBattle seerBattle;
    private final Screen returnTo;
    private final ResourceRepository<Image> ir = Main.IMAGE_REPOSITORY;
    private final ChangeListener<Item> handler = (p, o, n) ->
    {
        if (n != null)
        {
            performItemOperation(n);
        }
    };

    BattleScreen(GameSaveManager gsm, SeerBattle seerBattle, final Screen returnTo)
    {
        this(gsm, seerBattle, null, returnTo);
    }

    private BattleScreen(
        GameSaveManager gsm,
        SeerBattle seerBattle,
        User npc,
        final Screen returnTo)
    {
        this.npc = npc;
        this.gsm = gsm;
        this.seerBattle = seerBattle;
        this.returnTo = returnTo;
        Fxml.load(this);

        if (returnTo instanceof DebugScreen)
        {
            debugReadIn(new Scanner(System.in));
        }

        menuPane.visibleProperty().addListener(e ->
        {
            if (menuPane.isVisible() && checkExit())
            {
                gameEnd();
            }
        });

        new KeyBinder(KeyEvent.KEY_PRESSED)
            .bind(new KeyCodeCombination(KeyCode.ENTER), e ->
            {
                if (ft != null && ft.getCurrentTime().lessThan(ft.getDuration()))
                {
                    return;
                }
                if (menuPane.isVisible())
                {
                    performMenuOperate(menuPane.selectedOperation());
                }
                else if (skillPane.isVisible())
                {
                    skillPane.setPause(true);
                    performSkillOperation(skillPane.getCurrentSkill());
                }
            })
            .bind(new KeyCodeCombination(KeyCode.ESCAPE), e -> returnToMaster())
            .listenTo(this.sceneProperty());
    }

    @FXML
    private void initialize()
    {
        refreshInfo();
        dialogPane.addOnFinishedHandler(() ->
        {
            fadeOut(dialogPane);
            fadeIn(menuPane);
        });

        bagPane.addEventFilter(KeyEvent.KEY_PRESSED, e ->
        {
            if (e.getCode().equals(KeyCode.ESCAPE))
            {
                returnToMaster();
            }
            else if (e.getCode().equals(KeyCode.ENTER))
            {
                bagPane.getDecided().removeListener(handler);
                bagPane.getDecided().addListener(handler);
            }
        });
    }

    private void returnToMaster()
    {
        Component current = getCurrentVisibleComponent();
        if (current == skillPane || current == bagPane)
        {
            if (current.isLastPage())
            {
                fadeOut((Node)current, () -> fadeIn(menuPane));
            }
            else
            {
                current.goBack();
            }
        }
    }

    private void gameEnd()
    {
        menuPane.setOpacity(0);
        fadeOut(this, () -> switchTo(returnTo));
    }

    private void debugReadIn(Scanner scn)
    {
        //FIXME: Lock is required to prevent errors.
        Runnable rnb = () ->
        {
            User dummy = new User("Dummy Schafer", -1);
            System.out.println("Debug terminal has been enabled.");

            master:
            while (true)
            {
                System.out.print("Enter Commands: ");
                String commands = scn.nextLine().toLowerCase().trim();
                String[] optional = commands.split(" ");

                switch (commands)
                {
                    case "":
                        break;
                    case "help":
                        System.out.println("new npc - Add a dummy NPC into the current battle.");
                        System.out.println("rm npc - Remove NPC from the game.");
                        System.out.println("exit - Exit this terminal session.");

                        System.out.println(
                            "info - Display information of this current battle session.");

                        System.out.println("set ene hp - Set a new HP for enemy.");
                        System.out.println("set aly hp - Set a new HP for ally.");
                        System.out.println("ref scr - Refresh screen.");
                        break;
                    case "new npc":
                        System.out.println("A dummy NPC has been added.");
                        npc = dummy;
                        break;
                    case "ref scr":
                        Platform.runLater(this::refreshInfo);
                        System.out.println("Refreshed");
                        break;
                    case "rm npc":
                        System.out.println("Current NPC has been removed.");
                        npc = null;
                        break;
                    case "info":
                        System.out.println("NPC: " + (npc == null ? "Null" : npc.getNickname()));
                        System.out.println("Ally: " + seerBattle.getAlly().getNickname());

                        System.out.println("HP: " + seerBattle.getAlly().getHitPoints() + "/"
                            + seerBattle.getAlly().getMaxHitPoints());

                        System.out.println("Enemy: " + seerBattle.getEnemy().getNickname());

                        System.out.println("HP: " + seerBattle.getEnemy().getHitPoints() + "/"
                            + seerBattle.getEnemy().getMaxHitPoints());

                        break;
                    case "set ene hp":
                        System.out.print("Enter new HP: ");
                        String amount = scn.nextLine();
                        int i;
                        try
                        {
                            i = Integer.valueOf(amount);
                        }
                        catch (NumberFormatException e)
                        {
                            System.err.println("Invalid input, try again.");
                            break;
                        }

                        seerBattle.getEnemy()
                            .receiveAttack(seerBattle.getEnemy().getHitPoints() - i, null);

                        System.out.println("Enemy new HP: " + seerBattle.getEnemy().getHitPoints());
                        break;
                    case "set aly hp":
                        System.out.print("Enter new HP: ");
                        String amt = scn.nextLine();
                        int x;
                        try
                        {
                            x = Integer.valueOf(amt);
                        }
                        catch (NumberFormatException e)
                        {
                            System.err.println("Invalid input, try again.");
                            break;
                        }

                        seerBattle.getAlly()
                            .receiveAttack(seerBattle.getAlly().getHitPoints() - x, null);

                        System.out.println("Ally new HP: " + seerBattle.getAlly().getHitPoints());
                        break;

                    case "exit":
                        System.err.println("Terminal session ended, re-enter this screen to enable "
                            + "this feature again.");

                        break master;
                    default:
                        System.out.println("The command: " + commands + ": does not exist.");
                        System.out.println("Use \"help\" to view a list of available commands.");
                        break;
                }
            }
        };

        t = new Thread(rnb);
        t.start();
    }

    private Component getCurrentVisibleComponent()
    {
        if (menuPane.isVisible())
        {
            return menuPane;
        }
        else if (skillPane.isVisible())
        {
            return skillPane;
        }
        else if (bagPane.isVisible())
        {
            return bagPane;
        }
        else if (dialogPane.isVisible())
        {
            return dialogPane;
        }
        throw new UnsupportedOperationException("No Components is Visible.");
    }

    private boolean checkExit()
    {
        return checkExit(null);
    }

    private boolean checkExit(BattleOperation operation)
    {
        if (npc == null)
        {
            return seerBattle.isEnemyFainted() || operation == BattleOperation.Run;
        }

        return false;
    }

    private void performMenuOperate(BattleOperation operation)
    {
        if (operation == BattleOperation.Fight)
        {
            fadeOut(menuPane,
                () ->
                {
                    skillPane.setSeer(seerBattle.getAlly());
                    skillPane.display();
                    fadeIn(skillPane);
                }
            );
        }
        else if (operation == BattleOperation.Bag)
        {
            fadeOut(menuPane,
                () ->
                {
                    bagPane.setPlayer(gsm.gameSave().game().user());
                    bagPane.setBattle(seerBattle);
                    bagPane.display(npc != null);
                    fadeIn(bagPane);
                });
        }
        else if (operation == BattleOperation.Run)
        {
            List<String> messages = new ArrayList<>();
            if (checkExit(BattleOperation.Run))
            {
                messages.add(gsm.gameSave().game().user().getNickname() + " escaped.");
                dialogPane.replaceOnFinishedHandler(() ->
                    fadeOut(dialogPane, this::gameEnd));
            }
            else
            {
                if (npc != null)
                {
                    messages.add(npc.getNickname() + ": You can't get away from this!");
                }
                messages.add(gsm.gameSave().game().user().getNickname() + " is unable to escape.");
            }
            fadeOut(menuPane);
            fadeIn(dialogPane, () -> dialogPane.showMessages(messages));
        }
        else
        {
            System.out.println(operation.toString());
        }
    }

    private void performItemOperation(Item item)
    {
        final BattleCommand allyMove;
        if (item instanceof Capsule)
        {
            allyMove = new ItemCommand(seerBattle.getEnemy(), item, gsm.gameSave().game().user());
        }
        else
        {
            allyMove = new ItemCommand(seerBattle.getAlly(), item, gsm.gameSave().game().user());
        }

        TurnResult result = performCommand(allyMove);

        CommandResult firstRecord = result.getFirstResult();
        CommandResult secondRecord = result.getSecondResult();

        createMessage(firstRecord, secondRecord, bagPane);
    }

    private void createMessage(final CommandResult firstRecord,
                               final CommandResult secondRecord,
                               Node component)
    {
        var first = firstRecord.getPerformOrUseOnSeer();
        var second = secondRecord.getPerformOrUseOnSeer();

        List<String> messages = new ArrayList<>();

        messages.add(!firstRecord.wasExecuted()
            ? first.getNickname() + " isn't able to perform a move."
            : firstRecord.getOperation() == BattleOperation.Bag
            ? itemPerformString(firstRecord)
            : skillPerformString(firstRecord));
        //FIXME: First record is not guaranteed to be the one record the item performed.
        messages.add(!secondRecord.wasExecuted()
            ? second.getNickname() + (second.getHitPoints() <= 0
            ? " has fainted."
            : " isn't able to perform a move.")
            : secondRecord.getOperation() == BattleOperation.Bag
            ? itemPerformString(secondRecord)
            : skillPerformString(secondRecord));

        if (first.getHitPoints() <= 0)
        {
            messages.add(first.getNickname() + " has fainted.");
        }

        refreshInfo();
        fadeOut(component);
        fadeIn(dialogPane, () -> dialogPane.showMessages(messages));
    }

    private TurnResult performCommand(BattleCommand allyMove)
    {
        BattleCommand enemyMove = CoreML.getBattleCommand(
            seerBattle.getEnemy(),
            seerBattle.getAlly());

        return seerBattle.executeTurn(allyMove, enemyMove);
    }

    private void performSkillOperation(Skill skill)
    {
        if (seerBattle.getAlly().isSkillCallable(skill))
        {
            BattleCommand allyMove = new SkillCommand(
                skill,
                seerBattle.getAlly(),
                seerBattle.getEnemy());

            TurnResult result = performCommand(allyMove);

            CommandResult firstRecords = result.getFirstResult();
            CommandResult secondRecords = result.getSecondResult();

            createMessage(firstRecords, secondRecords, skillPane);
        }
        else
        {
            skillPane.setMessage("Current Skill is out of PP.", true);
            skillPane.setPause(false);
        }
    }

    private String specialSkillStatusString(CommandResult records)
    {
        String stringBuilder = "";

        if (records.isCriticalHit())
        {
            stringBuilder += "This is a critical hit. ";
        }

        if (records.isSelfHeal())
        {
            stringBuilder += records.getPerformOrUseOnSeer().getNickname() + " self healed. ";
        }

        if (records.isImpacted())
        {
            stringBuilder += records.getDefendSeer().getNickname() + " get Impacted. ";
        }

        return stringBuilder.trim();
    }

    private String skillPerformString(CommandResult records)
    {
        return ((records.getPerformOrUseOnSeer().getNickname()
            + " uses "
            + ((Skill)records.getMove()).getName()
            + " on "
            + records.getDefendSeer().getNickname()
            + ". "
            + specialSkillStatusString(records))).trim();
    }

    private String itemPerformString(CommandResult records)
    {
        Seer perform = records.getPerformOrUseOnSeer();
        Seer defend = records.getDefendSeer();
        return (((perform == null ? gsm.gameSave().game().user().getNickname()
            : perform.getNickname())
            + " uses "
            + ((Item)records.getMove()).getName()
            + " on "
            + (defend == null ? "itself" : defend.getNickname())
            + ". "
            + specialSkillStatusString(records))).trim();
    }

    private void refreshInfo()
    {
        enemyName.setText(seerBattle.getEnemy().getClass().getSimpleName());
        allyName.setText(seerBattle.getAlly().getClass().getSimpleName());

        enemyLevel.setText("LV: " + seerBattle.getEnemy().getCurLevel());
        allyLevel.setText("LV: " + seerBattle.getAlly().getCurLevel());

        allyHp.setText("HP: " + seerBattle.getAlly().getHitPointsPercentage() + "%");
        enemyHp.setText("HP: " + seerBattle.getEnemy().getHitPointsPercentage() + "%");

        enemy.setImage(getSeerImage(seerBattle.getEnemy()));
        ally.setImage(getSeerImage(seerBattle.getAlly()));
    }

    private void fadeOut(Node node)
    {
        fadeOut(node, null);
    }

    private void fadeOut(Node node, Runnable runnable)
    {
        if (node.isVisible())
        {
            ft = new FadeTransition(Duration.millis(150), node);
            ft.setFromValue(1.0);
            ft.setToValue(0.5);
            ft.setOnFinished(e ->
            {
                node.setVisible(false);
                if (runnable != null)
                {
                    runnable.run();
                }
            });
            ft.play();
        }
    }

    private void fadeIn(Node node)
    {
        fadeIn(node, null);
    }

    private void fadeIn(Node node, Runnable runnable)
    {
        if (!node.isVisible())
        {
            node.setVisible(true);
            ft = new FadeTransition(Duration.millis(150), node);
            ft.setFromValue(0.5);
            ft.setToValue(1.0);
            ft.setOnFinished(e ->
            {
                if (runnable != null)
                {
                    runnable.run();
                }
            });
            ft.play();
        }
    }

    private Image getSeerImage(Seer seer)
    {
        return ir.get("/images/seer/" + seer.getClass().getSimpleName().toLowerCase() + ".png");
    }
}
