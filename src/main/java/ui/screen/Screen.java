package ui.screen;

import javafx.scene.layout.StackPane;
import ui.animation.CanvasOperator;

public class Screen extends StackPane
{
    public Screen()
    {
        setPrefWidth(CanvasOperator.VIEW_WIDTH);
        setPrefHeight(CanvasOperator.VIEW_HEIGHT);
    }

    public void switchTo(Screen screen)
    {
        getScene().setRoot(screen);
    }
}
