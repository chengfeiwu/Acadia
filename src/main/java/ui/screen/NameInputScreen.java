package ui.screen;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import ui.Fxml;
import ui.KeyBinder;
import util.GameSaveManager;

public class NameInputScreen extends Screen
{
    @FXML private TextField name;

    private final GameSaveManager gameSaveManager;

    public NameInputScreen(final GameSaveManager gameSaveManager)
    {
        this.gameSaveManager = gameSaveManager;

        new KeyBinder(KeyEvent.KEY_PRESSED)
            .bind(new KeyCodeCombination(KeyCode.ENTER), event -> setNickname(), true)
            .listenTo(this.sceneProperty());

        Fxml.load(this);
    }

    @FXML
    private void initialize()
    {
        name.prefWidthProperty().bind(this.widthProperty());
        name.setPromptText(gameSaveManager.gameSave().game().user().getNickname());

        final var fadeIn = new FadeTransition(Duration.millis(500), this);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play();
    }

    @FXML
    private void setNickname()
    {
        if (!name.getText().equals(""))
        {
            gameSaveManager.gameSave().game().user().setNickname(name.getText());
        }

        switchTo(new PostSettingUpScreen(gameSaveManager));
    }
}
