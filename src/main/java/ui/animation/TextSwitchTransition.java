package ui.animation;

import javafx.animation.FadeTransition;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class TextSwitchTransition
{
    private Label label;
    private String text;
    private Duration duration;
    private Runnable run;
    private Double fadeInValue;

    public TextSwitchTransition(Label label, String text, Duration duration)
    {
        this.label = label;
        this.text = text;
        this.duration = duration;
        fadeInValue = label.getOpacity();
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void setOnFinished(Runnable e)
    {
        this.run = e;
    }

    public void setFadeInValue(double fadeInValue)
    {
        this.fadeInValue = fadeInValue;
    }

    public Duration getDuration()
    {
        return duration;
    }

    public void play()
    {
        FadeTransition fadeOut = new FadeTransition(duration.divide(2), label);
        fadeOut.setFromValue(label.getOpacity());
        fadeOut.setToValue(0.0);

        FadeTransition fadeIn = new FadeTransition(duration.divide(2), label);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(fadeInValue);

        fadeOut.play();
        fadeOut.setOnFinished(e ->
        {
            label.setText(text);
            fadeIn.play();
        });

        fadeIn.setOnFinished(e ->
        {
            if (run != null)
            {
                run.run();
            }
        });
    }
}
