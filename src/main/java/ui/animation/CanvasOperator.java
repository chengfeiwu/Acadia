package ui.animation;

import javafx.animation.PauseTransition;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.util.Duration;
import model.AirWall;
import model.Building;
import model.Bunker;
import model.Direction;
import model.Game;
import model.Map;
import model.Shop;
import model.Terrain;
import model.Tile;
import model.TileObject;
import model.Vector2D;
import ui.Main;

public class CanvasOperator
{
    public static final int TILE_COLUMNS_PER_VIEW = 14;
    public static final int TILE_ROWS_PER_VIEW = 10;
    public static final int TILE_WIDTH = 50;
    public static final int TILE_HEIGHT = 50;
    public static final int VIEW_WIDTH = TILE_COLUMNS_PER_VIEW * TILE_WIDTH;
    public static final int VIEW_HEIGHT = TILE_ROWS_PER_VIEW * TILE_HEIGHT;

    public static void mapCanvas(Game game, Canvas canvas)
    {
        int y = game.playerPosition().y();
        int x = game.playerPosition().x();
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        int shopCount = 0;

        if (x % TILE_COLUMNS_PER_VIEW != 0)
        {
            x = x / TILE_COLUMNS_PER_VIEW * TILE_COLUMNS_PER_VIEW;
        }
        if (y % TILE_ROWS_PER_VIEW != 0)
        {
            y = y / TILE_ROWS_PER_VIEW * TILE_ROWS_PER_VIEW;
        }

        Map map = game.map();
        for (int axisX = x; axisX < x + TILE_COLUMNS_PER_VIEW && axisX < map.width(); axisX++)
        {
            for (int axisY = y; axisY < y + TILE_ROWS_PER_VIEW && axisY < map.height(); axisY++)
            {
                Terrain lvSide = null;
                Terrain rvSide = null;
                Terrain uhSide = null;
                Terrain bhSide;

                int locX = axisX - (axisX / TILE_COLUMNS_PER_VIEW * TILE_COLUMNS_PER_VIEW);
                int locY = axisY - (axisY / TILE_ROWS_PER_VIEW * TILE_ROWS_PER_VIEW);
                Tile curr = map.tileAt(axisX, axisY);
                Terrain tr = curr.terrain();

                gc.drawImage(
                    Main.IMAGE_REPOSITORY.get(terrainToUrl(tr) + ".png"),
                    locX * TILE_WIDTH,
                    locY * TILE_HEIGHT);

                if (tr.equals(Terrain.SandRoad))
                {
                    if (axisX > 0)
                    {
                        lvSide = map.tileAt(axisX - 1, axisY).terrain();
                        terrainSides(lvSide, "lvSide", gc, locX, locY);
                    }
                    if (axisX < map.width() - 1)
                    {
                        rvSide = map.tileAt(axisX + 1, axisY).terrain();
                        terrainSides(rvSide, "rvSide", gc, locX, locY);
                    }
                    if (axisY < map.height() - 1)
                    {
                        bhSide = map.tileAt(axisX, axisY + 1).terrain();
                        terrainSides(bhSide, "bhSide", gc, locX, locY);
                    }
                    if (axisY > 0)
                    {
                        uhSide = map.tileAt(axisX, axisY - 1).terrain();
                        terrainSides(uhSide, "uhSide", gc, locX, locY);
                    }
                    if (lvSide != null && uhSide != null) //23px
                    {
                        if (lvSide.equals(Terrain.SandRoad))
                        {
                            Terrain leftUp = map.tileAt(axisX - 1, axisY - 1).terrain();
                            terrainSides(leftUp, "coverL", gc, locX, locY);
                        }
                    }
                    if (rvSide != null && uhSide != null) //23px
                    {
                        if (rvSide.equals(Terrain.SandRoad))
                        {
                            Terrain leftUp = map.tileAt(axisX + 1, axisY - 1).terrain();
                            terrainSides(leftUp, "coverR", gc, locX, locY);
                        }
                    }
                }

                TileObject to = curr.tileObject();
                if (to != null)
                {
                    if (to instanceof Shop || to instanceof Bunker)
                    {
                        shopCount++;
                        switch (shopCount)
                        {
                            case 1:
                                gc.drawImage(
                                    Main.IMAGE_REPOSITORY.get(tileObjToUrl(to) + "_lu.png"),
                                    locX * TILE_WIDTH,
                                    locY * TILE_HEIGHT);
                                break;
                            case 2:
                                gc.drawImage(
                                    Main.IMAGE_REPOSITORY.get(tileObjToUrl(to) + "_ld.png"),
                                    locX * TILE_WIDTH,
                                    locY * TILE_HEIGHT);
                                break;
                            case 3:
                                gc.drawImage(
                                    Main.IMAGE_REPOSITORY.get(tileObjToUrl(to) + "_ru.png"),
                                    locX * TILE_WIDTH,
                                    locY * TILE_HEIGHT);
                                break;
                            case 4:
                                gc.drawImage(
                                    Main.IMAGE_REPOSITORY.get(tileObjToUrl(to) + "_rd.png"),
                                    locX * TILE_WIDTH,
                                    locY * TILE_HEIGHT);
                                break;
                            default:
                                throw new UnsupportedOperationException();
                        }
                        if (shopCount == 4)
                        {
                            shopCount = 0;
                        }
                        continue;
                    }

                    gc.drawImage(
                        Main.IMAGE_REPOSITORY.get(tileObjToUrl(to) + ".png"),
                        locX * TILE_WIDTH,
                        locY * TILE_HEIGHT);
                }
            }
        }
    }

    public static void interactMap(final Game game, final Canvas canvas)
    {
        if (!game.map().tileAt(game.playerPosition()).hasTileObject())
        {
            throw new IllegalStateException(
                "No TileObject exists at tile: " + game.playerPosition());
        }

        final var tileObject = game.map().tileAt(game.playerPosition()).tileObject();
        final var context = canvas.getGraphicsContext2D();

        //Skip these blocks as they don't have a different view when interact.
        if (tileObject instanceof Building || tileObject instanceof AirWall)
        {
            return;
        }

        if (tileObject.isInteractive())
        {
            final var x = game.playerPosition().x() % TILE_COLUMNS_PER_VIEW;
            final var y = game.playerPosition().y() % TILE_ROWS_PER_VIEW;

            context.drawImage(Main.IMAGE_REPOSITORY.get(tileObjToUrl(tileObject) + "_inter.png"),
                x * TILE_WIDTH,
                y * TILE_HEIGHT);

            PauseTransition pause = new PauseTransition(Duration.millis(100));
            pause.play();
            pause.setOnFinished(e -> mapCanvas(game, canvas));
        }
        else
        {
            throw new UnsupportedOperationException("This TileObject is not able to interact");
        }
    }

    public static boolean willViewChange(final Game game, final Vector2D movement)
    {
        final int currentViewX = game.playerPosition().x() / TILE_COLUMNS_PER_VIEW;
        final int currentViewY = game.playerPosition().y() / TILE_ROWS_PER_VIEW;

        final var finalPosition = game.playerPosition().add(movement);
        int finalViewX = finalPosition.x() / TILE_COLUMNS_PER_VIEW;
        int finalViewY = finalPosition.y() / TILE_ROWS_PER_VIEW;

        return currentViewX != finalViewX
            || currentViewY != finalViewY;
    }

    public static void drawCharacter(
        final Direction direction,
        final Canvas canvas,
        final Game game)
    {
        final var context = canvas.getGraphicsContext2D();
        final var sprite = direction.name().toLowerCase() + "-0.png";

        context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        context.drawImage(
            Main.IMAGE_REPOSITORY.get("/images/character/" + sprite),
            game.playerPosition().x() % TILE_COLUMNS_PER_VIEW * TILE_WIDTH,
            game.playerPosition().y() % TILE_ROWS_PER_VIEW * TILE_HEIGHT);
    }

    private static String terrainToUrl(Terrain tr)
    {
        return String.format("/images/maps/terrain/%s", tr.toString().toLowerCase());
    }

    private static void terrainSides(Terrain tr, String side, GraphicsContext gc, int x, int y)
    {
        if (!tr.equals(Terrain.SandRoad))
        {
            gc.drawImage(
                Main.IMAGE_REPOSITORY.get(String.format("%s_%s.png", terrainToUrl(tr), side)),
                x * TILE_WIDTH,
                y * TILE_HEIGHT);
        }
    }

    private static String tileObjToUrl(TileObject to)
    {
        return "/images/maps/tileobj/" + to.getClass().getSimpleName();
    }
}
