package ui.animation;

import javafx.animation.Transition;
import javafx.scene.control.Labeled;
import javafx.util.Duration;

public class TypewriterTransition extends Transition
{
    private Labeled control;
    private String text;
    private int millisecondsPerCharacter;
    private Duration duration;

    public TypewriterTransition(Labeled control, String text)
    {
        this(control, text, 50);
    }

    public TypewriterTransition(Labeled control, String text, int millisecondsPerCharacter)
    {
        this.control = control;
        this.text = text;
        this.millisecondsPerCharacter = millisecondsPerCharacter;

        duration = Duration.millis(text.length() * this.millisecondsPerCharacter == 0
                ? 1
                : text.length() * this.millisecondsPerCharacter);

        setCycleDuration(duration);
    }

    public Duration getDuration()
    {
        return duration;
    }

    public void setText(String text)
    {
        duration = Duration.millis(text.length() * millisecondsPerCharacter);
        setCycleDuration(duration);
        this.text = text;
    }

    @Override
    protected void interpolate(double frac)
    {
        final int length = text.length();
        final int n = Math.round(length * (float)frac);
        control.setText(text.substring(0, n));
    }
}
