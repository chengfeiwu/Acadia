package ui.animation;

import javafx.animation.Transition;
import javafx.scene.canvas.Canvas;
import javafx.util.Duration;
import model.Direction;
import model.Vector2D;
import ui.Main;

public class CharacterMovingTransition extends Transition
{
    private static final Duration DURATION = Duration.millis(300);

    private final Canvas canvas;
    private final Direction direction;
    private final Vector2D initialPosition;

    public CharacterMovingTransition(
        final Canvas canvas,
        final Direction direction,
        final Vector2D initialPosition)
    {
        this.canvas = canvas;
        this.direction = direction;
        this.initialPosition = initialPosition;
        setCycleDuration(DURATION);
    }

    @Override
    protected void interpolate(final double frac)
    {
        final var context = canvas.getGraphicsContext2D();

        context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        final int frame = frame(frac);

        // `(x + y) % 2` provides a "checker board" distribution of 0/1,
        // meaning that we'll alternate the animation as we move in any direction.
        final boolean useAlternateAnimation = (initialPosition.x() + initialPosition.y()) % 2 == 1;

        // FIXME: Use proper sprite sheets
        final var framePath = String.format("/images/character/%s-%d%s.png",
            direction.name().toLowerCase(),
            frame,
            frame > 0 && useAlternateAnimation ? "-alt" : "");

        final var movement = direction.toVector2D();

        final int initialX = initialPosition.x() % CanvasOperator.TILE_COLUMNS_PER_VIEW
            * CanvasOperator.TILE_WIDTH;

        final int initialY = initialPosition.y() % CanvasOperator.TILE_ROWS_PER_VIEW
            * CanvasOperator.TILE_HEIGHT;

        context.drawImage(
            Main.IMAGE_REPOSITORY.get(framePath),
            initialX + movement.x() * frac * CanvasOperator.TILE_WIDTH,
            initialY + movement.y() * frac * CanvasOperator.TILE_HEIGHT);
    }

    private int frame(final double frac)
    {
        final double first = 0.13;
        final double second = 0.38;
        final double third = 0.63;
        final double fourth = 0.87;

        return frac < first ? 0
            : frac < second ? 1
            : frac < third ? 2
            : frac < fourth ? 1
            : 0;
    }
}
