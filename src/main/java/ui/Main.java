package ui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.GsonExclusion;
import model.Map;
import platform.Platform;
import ui.resource.CachedResourceRepository;
import ui.resource.ImageRepository;
import ui.resource.MapRepository;
import ui.resource.MediaRepository;
import ui.resource.ResourceRepository;
import ui.screen.CopyrightScreen;
import ui.screen.DebugScreen;
import ui.screen.IntroScreen;
import ui.screen.Screen;
import util.GameSaveManager;

public class Main extends Application
{
    private static final Gson GSON = new GsonBuilder()
        .setExclusionStrategies(new GsonExclusion(Map.class))
        .create();

    public static final ResourceRepository<Image> IMAGE_REPOSITORY =
        new CachedResourceRepository<>(new ImageRepository());

    public static final ResourceRepository<Media> MEDIA_REPOSITORY =
        new CachedResourceRepository<>(new MediaRepository());

    public static final ResourceRepository<Map> MAP_REPOSITORY =
        new CachedResourceRepository<>(new MapRepository(IMAGE_REPOSITORY));

    // FIXME: This shouldn't be static, nor should it be public.
    public static MediaPlayer backgroundMusic;

    public static void fadeOutBackgroundMusic()
    {
        final var timeline = new Timeline(new KeyFrame(
            Duration.millis(2400),
            new KeyValue(backgroundMusic.volumeProperty(), 0.0)));

        timeline.setOnFinished(t -> backgroundMusic.stop());
        timeline.play();
    }

    private final GameSaveManager gameSaveManager;

    public Main()
    {
        gameSaveManager = new GameSaveManager(Platform.getInstance(), GSON);
        try
        {
            gameSaveManager.save();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void start(final Stage stage)
    {
        Font.loadFont(this.getClass().getResourceAsStream("/fonts/SHPinscher-Regular.ttf"), 0);
        backgroundMusic = new MediaPlayer(MEDIA_REPOSITORY.get("media/bgm.mp3"));
        backgroundMusic.setCycleCount(Animation.INDEFINITE);

        stage.setTitle("");
        stage.setResizable(false);
        stage.getIcons().add(IMAGE_REPOSITORY.get("images/icon.png"));
        stage.initStyle(gameSaveManager.gameSave().getSettings().getStageStyle());

        final var scene = new Scene(new CopyrightScreen(gameSaveManager));
        scene.getStylesheets().add(Screen.class.getResource("Screen.css").toExternalForm());

        stage.setScene(scene);
        bindGlobalKeys(stage.getScene());
        stage.show();
    }

    private void bindGlobalKeys(final Scene scene)
    {
        final var debugKeyCodeCombination =
            new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN, KeyCombination.ALT_ANY);

        new KeyBinder(KeyEvent.KEY_PRESSED)
            //Start of Debug Method. ONLY FOR DEBUG BUILD!
            .bind(debugKeyCodeCombination, e ->
            {
                final var root = scene.getRoot();

                if (!(root instanceof DebugScreen
                    || root instanceof CopyrightScreen
                    || root instanceof IntroScreen))
                {
                    scene.setRoot(new DebugScreen((Screen)root, gameSaveManager));
                }
            })
            .bind(new KeyCodeCombination(KeyCode.L), e ->
            {
                final var style = toggleStageStyle((Stage)scene.getWindow());

                try
                {
                    gameSaveManager.gameSave().getSettings().setStageStyle(style);
                    gameSaveManager.save();
                }
                catch (IOException exception)
                {
                    exception.printStackTrace();
                }
            })
            .listenTo(scene);
    }

    private StageStyle toggleStageStyle(final Stage stage)
    {
        final var style = stage.getStyle() == StageStyle.DECORATED
            ? StageStyle.UNDECORATED
            : StageStyle.DECORATED;

        final var toggled = new Stage(style);
        toggled.setTitle(stage.getTitle());
        toggled.setResizable(stage.isResizable());
        toggled.getIcons().add(IMAGE_REPOSITORY.get("images/icon.png"));

        final var scene = stage.getScene();
        stage.close();

        toggled.setScene(scene);
        toggled.show();

        return style;
    }

    public static void main(final String[] args)
    {
        launch(args);
    }
}
