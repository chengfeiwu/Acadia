package ui;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public class Fxml
{
    public static void load(final Parent parent)
    {
        final var name = parent.getClass().getSimpleName();

        try
        {
            // Convention-based FXML loading. The FXML file is expected
            // to have the same file name as the screen class.
            Fxml.load(name + ".fxml", parent);
        }
        catch (final IOException e)
        {
            // Since FXML loading is convention based, an `IOException`
            // most likely means that there's a programming bug. As a
            // result, we convert the `IOException` into an unhandled
            // `RuntimeException` - there's no sensible way for the
            // program to recover, we simply need to fix the bug.
            throw new RuntimeException(e);
        }

        // Convention-based CSS loading. The CSS file is expected to
        // have the same file name as the screen class. If the file
        // does not exist, then no stylesheet is added to the scene.
        final var cssUrl = parent.getClass().getResource(name + ".css");
        if (cssUrl != null)
        {
            parent.getStylesheets().add(cssUrl.toExternalForm());
        }
    }

    // By default, JavaFX wants developers to first load the FXML file, which will
    // then cause an instance of the FXML file's controller class to be constructed.
    // We want to flip this so that we write instances of controllers in code like
    // any other class, and then can load its associated FXML template afterward.
    // This method allows us to easily do that.
    private static void load(final String fxmlPath, final Object controller) throws IOException
    {
        final var loader = new FXMLLoader(controller.getClass().getResource(fxmlPath));
        loader.setRoot(controller);
        loader.setController(controller);
        loader.load();
    }
}
