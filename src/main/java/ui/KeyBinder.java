package ui;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

public class KeyBinder
{
    private EventType<KeyEvent> eventType;
    private Map<KeyCombination, Set<EventHandler<KeyEvent>>> keyBinds;
    private Set<EventHandler<KeyEvent>> anyKeyBinds;

    public KeyBinder(EventType<KeyEvent> eventType)
    {
        this.eventType = eventType;
        keyBinds = new HashMap<>();
        anyKeyBinds = new HashSet<>();
    }

    public KeyBinder bind(
        KeyCombination combination,
        EventHandler<KeyEvent> eventHandler,
        boolean onlyFireOnce)
    {
        Set<EventHandler<KeyEvent>> handlers = keyBinds.getOrDefault(combination, new HashSet<>());

        EventHandler<KeyEvent> fireOnceHandler = new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                eventHandler.handle(event);
                handlers.remove(this);
            }
        };

        handlers.add(onlyFireOnce ? fireOnceHandler : eventHandler);
        keyBinds.put(combination, handlers);
        return this;
    }

    public KeyBinder bind(KeyCombination combination, EventHandler<KeyEvent> eventHandler)
    {
        return bind(combination, eventHandler, false);
    }

    public KeyBinder bindAnyKey(EventHandler<KeyEvent> eventHandler, boolean onlyFireOnce)
    {
        EventHandler<KeyEvent> fireOnceHandler = new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                eventHandler.handle(event);
                anyKeyBinds.remove(this);
            }
        };

        anyKeyBinds.add(onlyFireOnce ? fireOnceHandler : eventHandler);
        return this;
    }

    public KeyBinder bindAnyKey(EventHandler<KeyEvent> eventHandler)
    {
        return bindAnyKey(eventHandler, false);
    }

    public KeyBinder unbind(KeyCombination combination, EventHandler<KeyEvent> eventHandler)
    {
        if (keyBinds.containsKey(combination))
        {
            keyBinds.get(combination).remove(eventHandler);
        }

        return this;
    }

    public KeyBinder unbind(KeyCombination combination)
    {
        keyBinds.put(combination, new HashSet<>());
        return this;
    }

    public KeyBinder unbindAnyKey(EventHandler<KeyEvent> eventHandler)
    {
        anyKeyBinds.remove(eventHandler);
        return this;
    }

    private EventHandler<KeyEvent> onEvent = event ->
    {
        for (KeyCombination keyBind : keyBinds.keySet())
        {
            if (keyBind.match(event))
            {
                for (EventHandler<KeyEvent> handler : keyBinds.get(keyBind))
                {
                    handler.handle(event);
                }
            }
        }

        for (EventHandler<KeyEvent> handler : anyKeyBinds)
        {
            handler.handle(event);
        }
    };

    public KeyBinder listenTo(Scene scene)
    {
        scene.addEventHandler(eventType, onEvent);
        return this;
    }

    public KeyBinder listenTo(ObservableValue<Scene> observable)
    {
        if (observable.getValue() != null)
        {
            listenTo(observable.getValue());
        }

        observable.addListener((p, oldScene, newScene) ->
        {
            if (oldScene != null)
            {
                stopListeningTo(oldScene);
            }

            if (newScene != null)
            {
                listenTo(newScene);
            }
        });

        return this;
    }

    private KeyBinder stopListeningTo(Scene scene)
    {
        scene.removeEventHandler(eventType, onEvent);
        return this;
    }
}
