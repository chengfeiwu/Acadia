package model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MapTests
{
    @Test
    public void getHeight_whenEmpty_zero()
    {
        Map map = new Map(new Tile[0][0]);

        assertEquals(0, map.height());
    }
}
